<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<section class="poli clearfix">
	<h1>Aviso de privacidad clientes y proveedores</h1>
	
	<p>AVISO DE PRIVACIDAD PARA GRUPO PGLM, S.A. DE C.V. Y/O CUALQUIERA DE SUS EMPRESAS FILIALES, SUBSIDIARIAS Y/O QUE PERTENECEN O SON CONTROLADAS POR EL MISMO GRUPO PGLM, S.A. DE C.V. (EN ADELANTE PAK2GO).</p>
	
	<p class="center"><b>AVISO DE PRIVACIDAD</b></p>
	
	<p class="tit">I. Identificación del responsable y tratamiento de sus datos personales.</p>
	
	<p>De conformidad con lo establecido en el Artículo 17, fracción II de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares le informamos que GRUPO PGLM, S.A. DE C.V. y/o cualquiera de sus empresas filiales, subsidiarias y/o que pertenecen o son controladas por el mismo Grupo PAK2GO (en adelante PAK2GO) (el "Responsable"), con domicilio en la calle Callejón del Mármol 215-A, Praderas de Santa Catarina, Nuevo León, C.P. 66364. (la "Dirección de Contacto") tratará los datos personales que recabe de usted en los términos del presente aviso de privacidad.</p>
	
	<p class="tit">II. Finalidades.</p>
	
	<p>El tratamiento se hará con las siguientes finalidades:</p>
	
	<p>a. Necesarias para la relación jurídica con el Responsable:</p>
	
	<div class="indent">
		<ol class="romanos">
			<li>Evaluarlo para su contratación;</li>
			<li>Tener certeza jurídica.</li>
			<li>Para la Facturación.</li>
			<li>Para prestar el servicio.</li>
		</ol>
	</div>
	
	<p>b. Otras:</p>
	
	<p class="tit">II. Efectuar transferencias con sus datos personales a terceros en términos del aviso de privacidad.</p>
	
	<p>Por lo tanto y para alcanzar las finalidades antes expuestas, se tratarán los siguientes datos personales: razón social, domicilio, datos de registro, RFC, acta constitutiva o datos de la misma, comprobante de domicilio, así como los datos del representante legal, como es el nombre, apellido paterno, apellido materno, nacionalidad, escolaridad, comprobante de domicilio, identificación oficiales.</p>

	<p class="tit">III. Opciones para limitar el uso o divulgación de sus datos personales.</p>

	<p>El Responsable ha designado a un encargado de datos personales (el "Oficial de Privacidad"), por lo tanto usted podrá limitar el uso o divulgación de sus datos personales de manera personal ante el Responsable, mediante solicitud que deberá presentar en la Dirección de Contacto.</p>

	<p class="tit">IV. Medios para revocar el consentimiento para el tratamiento de datos personales.</p>

	<p>Usted podrá revocar su consentimiento para el tratamiento de sus datos personales mediante una solicitud por escrito dirigida al Oficial de Privacidad a la Dirección de Contacto, salvo en el caso en que el Responsable haya solicitado mediante medios electrónicos de comunicación el consentimiento para el tratamiento de sus datos personales, en cuyo caso podrá usted revocar su consentimiento mediante un correo electrónico a la siguiente dirección <a href="mailto:datos.personales@pak2go.com">datos.personales@pak2go.com</a>.</p>

	<p class="tit">V. Medios para ejercer los derechos ARCO.</p>

	<p>Usted tiene el derecho de: (i) acceder a sus datos personales en nuestro poder y conocer los detalles del tratamiento de los mismos, (ii) rectificarlos en caso de ser inexactos o incompletos, (iii) cancelarlos cuando considere que no se requieren para alguna de las finalidades señaladas en el presente aviso de privacidad, estén siendo utilizados para finalidades no consentidas o haya finalizado la relación contractual o de servicio, o (iv) oponerse al tratamiento de los mismos para fines específicos, según lo diga la ley, (conjuntamente, los "Derechos ARCO").</p>

	<p>Para el ejercicio de sus Derechos ARCO, deberá presentar una solicitud (la "Solicitud ARCO"), al Responsable, a la atención del Oficial de Privacidad, a la Dirección de Contacto, acompañada de la siguiente información y documentación:</p>

	<div class="indent">
		<ol class="letras">
			<li>Su nombre, domicilio y correo electrónico para poder comunicarle la respuesta a la Solicitud ARCO;</li>
			<li>Una copia de los documentos que acrediten su identidad (copia de IFE, pasaporte o cualquier otra identificación oficial vigente) o en su caso, los documentos que acrediten su representación legal, cuyo original deberá presentar para poder recibir la respuesta del Responsable;</li>
			<li>Una descripción clara y precisa de los datos personales respecto de los cuales busca ejercer alguno de los Derechos ARCO;</li>
			<li>Cualquier documento o información que facilite la localización de sus datos personales, y;</li>
			<li>En caso de solicitar una rectificación de sus datos personales, deberá de indicar también, las modificaciones a realizarse y aportar la documentación que sustente su petición.</li>
		</ol>
	</div>

	<p>El Oficial de Privacidad responderá su Solicitud ARCO y los motivos de su decisión mediante un correo electrónico en un plazo máximo de 20 días hábiles contados desde el día en que se haya recibido su Solicitud ARCO. En caso de que la Solicitud ARCO se conteste de manera afirmativa o procedente, los cambios solicitados se harán en un plazo máximo de 15 días hábiles. El Responsable podrá notificarle dentro de los plazos referidos en este párrafo la prórroga de los mismos, por una sola vez, por un periodo igual al original.</p>

	<p>El Responsable podrá negar el acceso (la "Negativa") para que usted ejerza sus Derechos ARCO, en los supuestos que lo permita la ley, por lo que deberá informar a usted el motivo de tal decisión.</p>

	<p>La Negativa podrá ser parcial, en cuyo caso el Responsable efectuará el acceso, rectificación, cancelación u oposición en la parte procedente.</p>

	<p>El ejercicio de los Derechos ARCO será gratuito, pero si usted reitera su solicitud en un periodo menor a doce meses, los costos serán de tres días de Salario Mínimo General Vigente en el Distrito Federal, más I.V.A., a menos que existan modificaciones sustanciales al Aviso de Privacidad, que motiven nuevas Solicitudes ARCO. Usted deberá de cubrir los gastos justificados de envío o el costo de reproducción en copias u otros formatos y, en su caso, el costo de la certificación de documentos.</p>

	<p class="tit">VI. Cambios o modificaciones al aviso de privacidad.</p>

	<p>El Responsable se reserva el derecho de efectuar en cualquier momento modificaciones o actualizaciones al presente aviso de privacidad, en el entendido de que toda modificación al mismo se le hará conocer a usted por medio de la publicación de un aviso en la página de internet del Responsable, por lo que le recomendamos verificarla con frecuencia.</p>

	<p>En caso de que ocurra una vulneración de seguridad en cualquier fase del tratamiento de datos personales, que afecte de forma significativa sus derechos patrimoniales o morales, el Oficial de Privacidad le comunicará de forma inmediata por correo electrónico el suceso de vulneración de seguridad, para que usted pueda tomar las medidas necesarias correspondientes para la defensa de sus derechos. En caso de no contar con su correo electrónico, la notificación se publicará en la página de internet del Responsable.</p>

	<p class="tit">VII. Transferencias de sus datos personales.</p>

	<p>Le informamos que sus datos personales podrán ser transferidos dentro y fuera del país, solo a las Empresas del mismo grupo del Responsable, siempre que sea necesario para prestar los servicios acordados entre las partes.</p>

	<p>Sus datos personales no serán transferidos a terceros ajenos al Responsable.</p>

	<p class="tit">VIII. COOKIES</p>

	<p>Les informamos que PAK2GO utiliza cookies con diferentes fines, incluido el almacenamiento de información requerida para determinar la mejor forma de comunicarnos con los sistemas e identificar usuarios del sitio. Una cookie es un pequeño fichero con información que un servidor Web almacena temporalmente a través de su navegador Web, para utilizarla más tarde. La finalidad de la cookie no es que el servidor Web acceda y utilice la información privada sobre usted, pero si usted proporciona esa información personal, ésta quedará grabada en la cookie. También, puede limitar la función de la cookie si no desea que PAK2GO tenga acceso a través de éstas.</p>

	<p class="tit">IX. SEGURIDAD</p>

	<p>Le informamos que PAK2GO utiliza precauciones de seguridad, técnicas y organizacionales, con el fin de proteger su información de manipulación, pérdida, destrucción o acceso de personas no autorizadas. Cualquier información personal que se le facilite a PAK2GO será codificada en tránsito para prevenir el posible mal uso por terceras personas. Nuestros procedimientos de seguridad se revisan continuamente, tomando como base los nuevos desarrollos tecnológicos.</p>

	<p>Descargar el <a href="descargas/fs-arco.xls" target="_blank">Formato de Solicitud ARCO</a></p>
</section>

<?php include('footer.php'); ?>

</body>
</html>