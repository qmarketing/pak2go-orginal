<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Comercio Exterior</title>

<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<div class="cExteriorBanner">
	<h1>Comercio <br>Exterior</h1>
	<a href="#"></a>
</div>

<div class="clearfix">
<section class="asesoriaComercio">
	<article class="as1">
		<h4>ESTUDIO</h4>
		<br/><br/>
		<span></span>
		<h3>¿Buscas saber cuanto costaría TU producto puesto en TU bodega en México?  <br>Si asi lo es, requerimos los siguientes datos:</h3>
		<ul>
			<li>Cotización / Orden (para entender requerimiento)</li>
			<li>Ficha Técnica / Fotos  (para clasificar tu mercancia)</li>
			<li>Lista de Empaque (para calcular Logística)</li>
			<li>Contacto</li>
		</ul>
	</article>
	
	<article class="as2">
				<h4>COORDINACIÓN DE COMERCIO EXTERIOR <br />(sin Padrón de Importadores)</h4>
		<br /><br />
		<span></span>
		<h3>¿Buscas apoyo INTEGRAL en tu operación de Comercio Exterior, personalizado a tu necesidades? <br>Si asi lo es, tenemos la solución para ti:</h3>
		<ul>
			<li>Contactamos a tu Proveedor (programamos orden, cumplimiento con normas, pagos, documentacion, entrega, y posible negociacion)</li>
			<li>Operamos Logística Internacional (programamos recoleccion, despacho, entrega en origen)</li> 			
			<li>Operamos Tramite de Importacion (programamos alta con A.A.,  documentos, notificación de pagos y  seguimiento experimentado)</li>
			<li>Operamos Logística Nacional (programamos entrega en destino)</li>
		</ul>
		<p>Nota: ¡Factura de empresas Mexicanas con desglose de servicios!</p>
	</article>
	
	<article class="as3">
	<h4>COORDINACIÓN DE COMERCIO EXTERIOR <br />(con Padrón de Importadores)</h4>
		<br/><br/>
		<span></span>
		<h3>¿Buscas subcontratar un departamento de Comercio Exterior, hecho a tu medida?<br>Si asi lo es, tenemos la solución para ti:</h3>
		<ul>
			<li>Contactamos a tu Proveedor (programamos orden, cumplimiento con normas, documentacion, entrega)</li>
			<li>Operamos Logística Internacional (programamos recoleccion, despacho, entrega en origen)</li>
			<li>Operamos Tramite de Importacion (programamos alta con A.A.,  documentos, pagos y  seguimiento experimentado)</li>
			<li>Operamos Logística Nacional (programamos entrega en destino)</li>
			<p>Nota: ¡Todas la papeleria sale a nombre de tu empresa!</p>
		</ul>
	</article>

</section>

<section class="asesoriaComercio2 ">
	<article class="as1">
		<h4>RECURRENCIA EN COMERCIO EXTERIOR (clientes frecuentes)</h4>
		<br /><br />
		<span></span>
		<h3>¿Buscas apoyo en tu operación de Comercio Exterior empresarial?<br>Si asi lo es, tenemos la solución para ti:</h3>
		<ul>
			<li>Recopilamos y Preparamos Documentación (guiamos al cliente a organizar requerimientos informativos)</li>
			<li>Operamos Logística Internacional (programamos recoleccion, despacho, entrega en origen)</li>
			<li>Apoyamos en Tramite de Importacion (programamos alta con A.A.,  documentos, notificación de pagos y  seguimiento experimentado)</li>
			<li>Operamos Logística Nacional (programamos entrega en destino)</li>
			<p>Nota: ¡Todas la papeleria sale a nombre de tu empresa!</p>
		</ul>
	</article>
	
	<article class="as2">
		<h4>ALIANZA COMERCIAL WCA</h4>
		<br /><br />
		<span></span>
		<h3>¿Buscas apoyo en tu operación con Mexico?<br>Si asi lo es, tenemos la solución para ti:</h3>
		<ul>
			<li>Recopilamos y Preparamos Documentación (organizamos requerimientos informativos)</li>
			<li>Supervisamos tu Logística Internacional (guiamos con proceso a su llegada a puerto; gastos en puerto destino)</li>
			<li>Apoyamos en Tramite de Importacion (programamos alta con A.A.,  documentos, notificación de pagos y  seguimiento experimentado)</li>
			<li>Operamos Logística Nacional (programamos entrega en destino)</li>
		</ul>
	</article>
	
	<article class="as3">
		<h4>CERTIFICACIÓNES</h4>
		<br /><br />
		<span></span>
		<h3>¿La fracción que clasificamos tiene requisición de Certificación? <br>Si asi lo es:</h3>
		<ul>
			<li>Te guiamos en entender y/o procesar los tramites necesarios para cumplir con todas las restricciones detalladas.</li>
		</ul>
	</article>
</section>
<section class="asesoriaComercio2 ">
</section>



<section class="ComercioInfo">
	<div class="cont clearfix">
	<article class="tc1 clearfix">
		<h4>VERIFICACIÓN EN ORIGEN (Asia y Sudeste Asiático)</h4>
		<div class="all">
			<p>¿Buscas corroborar existencia de empresas, calidad, empaque, alcance empresarial de tu Proveedor?</p>
			<div class="col1">
				<h5>Opción 1</h5>
				<ul>
					<li>Verificamos físicamente con visita a la empresa para asegurarnos del domicilio que nos dieron.</li>
					<li>Verificamos físicamente con inspección de producto.</li>
					<li>(Cliente DEBE PROPORCIONAR características <br/> exactas de lo  que busca que chequemos.)</li>
				</ul>
			</div>
			
			<div class="col2">
				<h5>Opción 2</h5>
				<ul>
					<li>Verificamos, durante PRODUCCIÓN, que todas las especificaciones del producto y calidad se estén cumpliendo con tus expectativas.</li>
					<li>(Cliente nos DEBE PROPORCIONAR especificación exacta de lo que busca.)</li>
				</ul>
			</div>
			
			<div class="col3">
				<h5>Opcion 3</h5>
				<ul>
					<li>Verificamos la producción final y su calidad. </li>
					<li>Estar en el momento que se esta cargando tu contenedor, del cierre y sello de salida a embarque.</li>
					<li>(Cliente NOS DEBE PROPORCIONAR especificación exacta de empaque, calidad, color, etc.)</li>
				</ul>
			</div>
		</div>
			
	</article>
	</div>
</section>
</div>

<?php include('footer.php'); ?>

<script src="js/easing.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/exterior.js"></script>

<script>
	$(document).ready(function() {
		$('#l4').addClass("activadote");
	});
</script>
</body>
</html>