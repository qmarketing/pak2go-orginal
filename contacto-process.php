<?php
header('Content-Type: text/html; charset=UTF-8');

$EmailFrom = "contact@pak2go.com";
$EmailTo = "info@pak2go.com";
$Subject = "Mensaje de Pak2Go";

$Name = Trim(stripslashes($_POST['Name'])); 
$Tel = Trim(stripslashes($_POST['Tel'])); 
$Email = Trim(stripslashes($_POST['Email'])); 
$Message = Trim(stripslashes($_POST['Message']));
$Company = Trim(stripslashes($_POST['Company']));
$Subjects = Trim(stripslashes($_POST['Subjects']));
$Service = Trim(stripslashes($_POST['Service']));

// validation
$validationOK=true;
if (!$validationOK) {
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
  exit;
}

// prepare email body text
$Body = "";
$Body .= "Nombre: ";
$Body .= $Name;
$Body .= "\n\n";
$Body .= "Teléfono: ";
$Body .= $Tel;
$Body .= "\n\n";
$Body .= "Email: ";
$Body .= $Email;
$Body .= "\n\n";
$Body .= "Empresa: ";
$Body .= $Company;
$Body .= "\n\n";
$Body .= "Asunto: ";
$Body .= $Subjects;
$Body .= "\n\n";
$Body .= "Servicio de interes: ";
$Body .= $Service;
$Body .= "\n\n";
$Body .= "Comentarios: ";
$Body .= $Message;
$Body .= "\n\n";

// send email 
$success = mail($EmailTo, $Subject, $Body, "From: <$EmailFrom>");

// redirect to success page 
if ($success){
  print "<meta http-equiv=\"refresh\" content=\"0;URL=contacto.php#gracias\">";
}
else{
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
}
?>