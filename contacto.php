<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Mudanzas Fletes y Regresos</title>

<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<section class="contacto clearfix">
	<h2><span>{</span>Contacto<span>}</span></h2>
	
	<div class="cforma">
		<h3>Queremos escucharte.</h3>
		<p>Gracias por tu interés en contactarnos. Esperamos escuchar de ti, estamos abiertos a preguntas, comentarios, o ideas</p>
		
		<div class="gracias">Gracias, nos pondremos en contacto a la brevedad.</div>
		
		<div class="forma clearfix">
			<form method="post" action="contacto-process.php" name="formacoti">
				<span class="left">Nombre <input type="text" name="Name" id="Name"/></span>
				<span class="right">Empresa <input type="text" name="Company" id="Company" /></span>
				
				<span class="left">Telefono <input type="tel" name="Tel" id="Tel" /></span>
				<span class="right">Email <input type="text" name="Email" id="Email" /></span>
				
				<span class="left">Asunto <input type="text" name="Subjects" id="Subjects" /></span>
				<span class="right">Servicio de interes <input type="text" name="Service" id="Service" /></span>
				
				<span >Mensaje </span>
					
				<textarea name="Message" id="Message"></textarea>
				
				<div class="error_box"></div>
				
				<input type="submit" name="submit" value="ENVIAR MENSAJE" id="enviar">
			</form>
		</div>
	</div>
	
	<div class="location clearfix">
		<div class="locBlock lb1">
			<div class="icon"></div>
			<h4>Monterrey</h4>
			<p>Argentina 205-1<br>Col. Parque Industrial Martel, Santa Catarina, Nuevo León CP: 66367</p>
			<div class="tell"><span>Tel.: (81) 1365 5333</span></div>
		</div>
		
		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>D.F.</h4>
			<p>Calle del Rio # 8<br>Fracc. Industrial Alce Blanco, Naucalpan, Estado de México</p>
			<div class="tell"><span>Tel.: (55) 5531 3553</span></div>
		</div>
		
		<div class="locBlock lb3">
			<div class="icon"></div>
			<h4>Guadalajara</h4>
			<p>Pino Suarez #1039 bodega 25 <br>Col. El Vigía, CP 45145, Zapopan, Jalisco<p>
			<div class="tell"><span>Tel.: (33) 3615 6427</span></div>
		</div>
		
		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>Chihuahua</h4>
			<p>Paseo de Aldama 13906 <br>Col. Paseo de Chihuahua <br>CP 31205, Chihuahua, Chihuahua<p>
			<div class="tell"><span>Tel.: (614) 481 0497</span></div>
		</div>
		
		<div class="clear"></div>
		
		<div class="locBlock lb1">
			<div class="icon"></div>
			<h4>Reynosa</h4>
			<p>5 de Mayo  205C <br>Col. Bellavista entre Laredo y Veracruz, Reynosa, Tamps.<p>
			<div class="tell"><span>Tel.: (899) 925 5342</span></div>
		</div>
		
		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>León</h4>
			<p>Diego Velázquez 104-F <br>Col. Jardines de Oriente, CP: 37257, León, Guanajuato<p>
			<div class="tell"><span>Tel.: (477) 717 5906</span></div>
		</div>
		
		<div class="locBlock lb3">
			<div class="icon"></div>
			<h4>Córdoba</h4>
			<p>Av. 11 No 2001 Int.305 Piso 3 <br>Esquina Calle 20, Col. San Jose <br>Córdoba, Veracruz CP.94560<p>
			<div class="tell"><span>Tel.: (229) 293 9950</span></div>
		</div>
		
		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>Cd. Juárez</h4>
			<p>Calle Vicente Guerrero #5723 <br> Col. Alamos de San Lorenzo, CP 32340, Cd. Juárez, Chihuahua<p>
			<div class="tell"><span>Tel.: (656) 408 0520</span></div>
		</div>
		
		<div class="clear"></div>
		
		<div class="locBlock lb1">
			<div class="icon"></div>
			<h4>Manzanillo</h4>
			<p>Carr. libre a Colima km. 6.5 <br>Col. 5 de Mayo, C.P. 28800, Manzanillo, Colima<p>
			<div class="tell"><span>Tel.: 477 348 5926</span></div>
		</div>
		
		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>San Luis Potosí</h4>
			<p>Av. Chapultepec #1610 <br>Piso 1 Int. 3<br>Col. Desarrollo del Pedregal<br>San Luis Potosí, SLP 78295<p>
			<div class="tell"><span>Tel: (444) 274 0307 ext.5024</span></div>
		</div>
		
		<div class="locBlock lb3">
			<div class="icon"></div>
			<h4>Saltillo</h4>
			<p>Av. Universidad 1380<br>Col. Valle Universidad<br>Saltillo, Coahuila C.P. 25260<p>
			<div class="tell"><span>Tel: 844 285 17 05</span></div>
		</div>

		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>Queretaro</h4>
			<p> Nave Industrial No. 19 Norte, <br> Ubicada Dentro del Conjunto Industrial “P.K.Co “, <br>En carretera Estatal 431, Km. 1+300, Ejido El Colorado,<br> EL MARQUES,C.P 76246</p>
			<!-- <p>Av. 5 de Febrero No.1716<br>Zona Industrial Benito Juarez<br>Queretaro, Queretaro 76120<p> -->
			<div class="tell"><span>Tel. (442) 221 5528 </span></div>
		</div>

		
		<div class="clear"></div>

		<div class="locBlock lb1">
			<div class="icon"></div>
			<h4>Tijuana</h4>
			<p>Plaza Internacional Otay<br>Calle Bellas Artes 17686 Local 117A<br>Fraccionamiento Garita De Otay<br>Tijuana Baja California C.P. 22430<p>
			<div class="tell"><span>Tel. 664 647 5856</span></div>
		</div>
		
		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>PAK2GO LIMITED</h4>
			<p>Unit 702, 7/F, Bangkok Bank Building<br>No. 18 Bonham Strand West, Hong Kong<p>
			<div class="tell"><span>Tel.: (852) 2201 1066</span><br><span>Fax.: (852) 3105 0902</span></div>
		</div>
		
		<div class="locBlock lb3">
			<div class="icon"></div>
			<h4>Mexicali</h4>
			<p>Calzada Hector Teran Teran 2382 <br/>Col. Xochimilco Local 5 <br/>Mexicali, Baja California. CP 21380</p>
			<div class="tell"><span>Tel.: 686 561 7080</span></div>
		</div>

		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>Laredo</h4>
			<p>6553 Star Court<br>Laredo, TX, 78041<p>
			<div class="tell"><span>015Tel: +1 (956) 639  8015</span></div>
		</div>
		
		<div class="clear"></div>

		<div class="locBlock lb1">
			<div class="icon"></div>
			<h4>Culiacán</h4>
			<p>Calle Lesa 3282<br>Fraccionamiento Stanza Toscana<br>Culiacán Sinaloa C.P. 80050<p>
			<div class="tell"><span>Tel. 667 257 1559</span></div>
		</div>

		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>Puebla</h4>
			<p>Calzada Zavaleta No. 3913 2A <br>Col. Santa Cruz Buenavista, <br/>Puebla, Pue. C.P. 72154</p>
			<div class="tell"><span>Tel. 222 1 69 64 25</span></div>
		</div>

		<div class="locBlock lb3">
			<div class="icon"></div>
			<h4>Irapuato</h4>
			<p>Blvd Díaz Ordaz 2874 Despacho 401 <br/>Col. Las Reynas<br/>Irapuato, Guanajuato CP. 36660</p>
			<div class="tell"><span>Tel: 462 189 1098</span></div>
		</div>

		<div class="locBlock lb2">
			<div class="icon"></div>
			<h4>Torreon</h4>
			<p>Diagonal Reforma 1710 <br>Col. Centro,<br>Plaza Comercial Colibrí  CP 27000<p>
			<div class="tell"><span>Tel: 871 315 67 97</span></div>
		</div>

		

	</div>
</section>

<div id="map_canvas"></div>

<?php include('footer.php'); ?>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="js/mapa.js"></script>
<script type="text/javascript" src="js/validate.min.js"></script>
<script type="text/javascript" src="js/contacto.js"></script>
<script>
	$(document).ready(function() {
		$('#l6').addClass("activadote");
	});
</script>
</body>
</html>