<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<section class="poli clearfix">
	<h1>Derechos Arco y procedimiento</h1>
	
	<p>Los Derechos ARCO se definen como Derecho de acceso, rectificación, cancelación y oposición de los Titulares sobre sus datos personales. Los siguientes términos son los que se definen en la Ley de Datos y se utilizan en los avisos de privacidad de GRUPO PGLM, S.A. DE C.V. y/o cualquiera de sus empresas filiales, subsidiarias y/o que pertenecen o son controladas por el mismo grupo de Grupo PGLM, S.A. de C.V. (en adelante PAK2GO), que aparecen publicados en la página de internet:</p>

	<div class="indent">
		<ol class="numeros">
			<li>Derecho de acceso: El Titular tiene derecho a solicitar y obtener gratuitamente información de sus datos personales sometidos a tratamiento por el responsable de la Base de Datos, el origen de dichos datos, así como las comunicaciones realizadas o que se prevén hacer de los mismos.</li>
			<li>Derecho de cancelación: El Titular tiene derecho a solicitar y obtener gratuitamente la cancelación de sus datos personales cuando el tratamiento de los mismos no se ajuste a lo dispuesto en la Ley de Datos, o hayan dejado de ser necesarios o pertinentes para la finalidad para la cual hubieran sido recabados o registrados por el responsable de la Base de Datos o tratamiento.</li>
			<li>Derecho de oposición: Manifestación de voluntad del Titular que pone en conocimiento del responsable de la Base de Datos o tratamiento, su deseo de que sus datos personales no sean objeto de un concreto tratamiento.</li>
			<li>Derecho de rectificación: El Titular tiene derecho a solicitar y obtener gratuitamente la rectificación de sus datos personales inexactos o incompletos sometidos a tratamiento por el responsable.</li>
		</ol>
	</div>

	<p>Los Titulares o las personas físicas a quien correspondan los datos personales, podrán ejercer sus Derechos Arco bajo el siguiente procedimiento:</p>

	<div class="indent">
		<ol class="numeros">
			<li>Llenar la Solicitud Arco misma que se encuentra en la Página de Internet Formato de Solicitud ARCO</li>
			<li>Presentar la/las Solicitud/es Arco personalmente y debidamente firmada en la calle Callejón del Mármol 215-A, Praderas de Santa Catarina, Nuevo León, C.P. 66364.  No se recibirán Solicitudes ARCO por otros medios diferentes a lo establecido en el párrafo anterior.</li>
			<li>Con la finalidad de acreditar su identidad, el titular o la persona física deberá anexar a su solicitud original y copia de alguno de los siguientes documentos:</li>
			<ol class="letras">
				<li>Pasaporte vigente;</li>
				<li>Credencial para votar vigente expedida por el Instituto Federal Electoral;</li>
				<li>Cédula profesional expedida por la Secretaría de Educación Pública;</li>
				<li>Cartilla del Servicio Militar Nacional expedida por la Secretaría de Defensa Nacional;</li>
				<li>Documento migratorio vigente emitido por la autoridad competente (tratándose de extranjeros);</li>
				<li>Certificado de Matrícula Consular expedida por la Secretaría de Relaciones Exteriores o por la Oficina Consular correspondiente;</li>
				<li>Firma electrónica amparada por un certificado digital vigente; y</li>
				<li>Otros mecanismos de autenticación previamente acordados entre el titular y el responsable.</li>
			</ol>
			<li>En caso de que presente la Solicitud ARCO por medio de un representante legal se deberá anexar a la misma una carta firmada por el Titular donde señale expresamente que dicho representante legal está autorizado para hacer valer los Derechos ARCO por cuenta y nombre del Titular. Dicha carta deberá contener anexo alguno de los documentos de identificación del Titular previamente mencionados en el número dos. Adicionalmente, el representante legal, deberá anexar una carta poder firmada por 2 testigos.<br>Nota: Para la atención de las Solicitud ARCO, el Titular deberá cubrir únicamente los gastos de envío, reproducción y certificación de documentos (en caso de ser aplicable). Si el Titular realiza más de una Solicitud ARCO en un periodo menor a un año, los costos por parte de la Empresa no podrán ser mayores a 3 días de salario mínimo a menos que existan modificaciones sustanciales al aviso de privacidad que motiven nuevas consultas.</li>
			<li>Una vez recibida la Solicitud ARCO, los Oficiales de Datos Personales verificarán si ésta cuenta con todos los requisitos descritos previamente, de no ser así, contactará al Titular mediante correo electrónico y le requerirá corregir las deficiencias en los siguientes 5 días hábiles a la recepción de la solicitud. En caso de que la misma no sea corregida transcurridos 10 días hábiles, la solicitud se tendrá por no presentada.</li>
			<li>Cada una de las Solicitudes ARCO que reciba PAK2GO, contarán con la asignación de números de folio por tipo de solicitud ya sea solicitud de acceso, rectificación, cancelación u oposición.</li>
		</ol>
	</div>

	<p>La entidad no estará obligada a cancelar los datos personales cuando:</p>
	<ul>
		<li>Se refiera a las partes de un contrato y sean necesarios para su desarrollo y cumplimiento</li>
		<li>Deban ser tratados por disposición legal</li>
		<li>Obstaculice actuaciones judiciales o administrativas</li>
		<li>Sean necesarios para proteger los intereses jurídicamente tutelados del Titular</li>
		<li>Sean necesarios para cumplir con una obligación legalmente adquirida</li>
		<li>Sean objeto de tratamiento para la prevención o para el diagnóstico médico o la gestión de servicios de salud (siempre que dicho tratamiento se realice por un profesional de la salud).</li>
	</ul>

	<p>Nota: En caso de que la respuesta otorgada al Titular no lo satisfaga totalmente, éste podrá interponer el procedimiento de protección de datos personales ante el Instituto Federal de Acceso a la Información y Protección de Datos, de acuerdo a lo descrito en los procedimientos de privacidad de datos.</p>

	<p>Descargar el <a href="descargas/fs-arco.xls" target="_blank">Formato de Solicitud ARCO</a></p>
</section>

<?php include('footer.php'); ?>

</body>
</html>