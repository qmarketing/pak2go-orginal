<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>


<section class="empresa">
	<div class="empresaNav">
		<a href="#" class="clickQuienes">Quienes somos</a>
		<a href="#" class="clickMVV">Misión, Visión y Valores</a>
		<a href="#" class="clickHistoria">Historia</a>
		<a href="#" class="clickClientes">Clientes</a>
	</div>
	
 	<div class="empresaFrase">
		<h2><span>{</span>Encantados de Conocerte<span>}</span></h2>
	</div>
	
	<div class="quienesSomos">
		<article class="info clearfix">
			<h3><img src="images/quote1.png"/>Pak2Go Logistics nace a raíz de la visión de ofrecer soluciones logísticas integrales, creando así un abanico de opciones con la que se buscan 2 principales objetivos: la satisfacción del cliente y la rentabilidad de las empresas con las que trabajamos.<img src="images/quote2.png"/></h3>
			<div class="masInfo"><p>En Pak2Go Logistics estamos consientes que cada uno de nuestros clientes tienen diferentes necesidades por lo que siempre buscamos en conjunto las mejores opciones para crear un traje a la medida para así poder satisfacer las necesidades de cada uno de ellos.</p></div>
			<div class="socio">
				<h4>Tu socio logístico</h4>
				<p>Ofrecemos la mejor solución en almacenaje, distribución y logística. Realmente más que una solución, ofrecemos ser un verdadero socio logístico de tu empresa.</p>
			</div>
			<div class="objetivo">
				<h4>Objetivo</h4>
				<p>Nuestro objetivo es que nuestros clientes se enfoquen en incrementar sus ventas dejando su operación logística en manos de un experto.</p>
			</div>
		</article>
	</div>
	
	<div class="mvv clearfix">
		<article class="info clearfix">
			<div class="sencillo mvvblock"><h3>Sencillo</h3></div>
			<div class="economico mvvblock"><h3>Económico</h3></div>
			<div class="rapido mvvblock"><h3>Rápido</h3></div>
		</article>
		<div class="cont">
			<article class="info2">
				<div class="mision">
					<h4>Misión</h4>
					<!-- <p>Transformar el departamento del comercio global con soluciones óptimas de logística.</p> -->
					<p>Transformar el departamento logístico de nuestros clientes en algo. </p>
					<p><b>SER: Sencillo, Económico y Rápido.</b></p>
				</div>
				
				<div class="vision">
					<h4>Visión</h4>
					<p>Eliminar fronteras del comercio global con soluciones óptimas de logística.</p>
				</div>
			</article>
			<div class="info3">
				<h4 id="valores">Valores</h4>
				<ul>
					<li><span>Tenacidad:</span><p>Levantarse y luchar todos los días en contra de las adversidades.</p></li>
					<li><span>Pro Actividad:</span><p>Tomar acción sobre las oportunidades que se nos presentan a diario.</p></li>
					<li><span>Actitud positiva:</span><p>Enfrentar las dificultades con buen ánimo y perseverancia</p></li>
					<li><span>Colaboración:</span><p>El trabajo de varias personas en conjunto para conseguir un resultado muy difícil.</p></li>
					<li><span>Excelencia:</span><p>Buscar mejorar en todos los aspectos.</p></li>
				</ul>				
			</div>
		</div>
	</div>
	
	<div class="history">
		<div class="info clearfix">
			<span class="titulo"><img src="images/i-historia.png" alt="Historia"/></span>
			<a href="#" class="hprev"></a>
			<a href="#" class="hnext"></a>
			<div class="hslide">
				<div class="hblock clearfix">
					<span class="fecha">2008</span>
					<div class="htxt h2008">
						<p>
							Iniciamos operaciones en una oficina pequeña en Monterrey.<br><br>
							Personal: 3<br>
							Clientes: 30<br>
							Sucursales: 1
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2009</span>
					<div class="htxt h2009">
						<p>
							Apertura de plaza en Guadalajara.<br>
							En MTY nos cambiamos de la oficina pequeña a una bodega ofreciendo servicio de almacenaje en esta ciudad.<br><br>
							Personal: 10<br>
							Clientes: 260<br>
							Sucursales: 2
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2010</span>
					<div class="htxt h2010">
						<p>
							Apertura Plaza en Chihuahua, Reynosa, Leon.<br>
							Cambio de oficinas en GDL a bodega ofreciendo servicio de almacenaje en esta ciudad.<br><br>
							Personal: 25<br>
							Clientes: 520<br>
							Sucursales: 5
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2011</span>
					<div class="htxt h2011">
						<p>
							Apertura de Plaza en Mexico DF.<br>
							Cambio de oficinas en CHIHUAHUA a bodega ofreciendo servicio de almacenaje en esta ciudad.<br><br>
							Personal: 45<br>
							Clientes: 900<br>
							Sucursales: 6
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2012</span>
					<div class="htxt h2012">
						<p>
							Apertura de Pak2Go Internacional - Inicia cobertura mundial (Import / Export)<br>
							Apertura de Plaza en VERACRUZ.<br>
							Cambio de oficinas en MEXICO DF a bodega ofreciendo servicio de almacenaje en esta ciudad.<br><br>
							Personal: 75<br>
							Clientes: 1500<br>
							Sucursales: 7
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2013</span>
					<div class="htxt h2013">
						<p>
							Apertura de oficina en Hong Kong Pak2go Limited<br><br>
							Personal : 120<br>
							Clientes: 2,000<br>
							Sucursales: 10
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="greatplace"><img class="logo2" src="../images/greatplace.jpg" alt=""></div>
	</div>
	
	<div class="clientes clearfix">
		<div class="clblock b-abajo b-derecha"><img src="images/cl-aoc.png" alt="cl-aoc" width="138" height="46" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-cityclub.png" alt="cl-cityclub" width="98" height="99" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-totalchef.png" alt="cl-totalchef" width="130" height="107" /></div>
		<div class="clblock b-abajo"><img src="images/cl-kenworth.png" alt="cl-kenworth" width="178" height="30" /></div>
		
		<div class="clblock b-abajo b-derecha"><img src="images/cl-photofolio.png" alt="cl-photofolio" width="154" height="34" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-sigma.png" alt="cl-sigma" width="145" height="83" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-soriana.png" alt="cl-soriana" width="176" height="48" /></div>
		<div class="clblock b-abajo"><img src="images/cl-tecdemty.png" alt="cl-tecdemty" width="175" height="64" /></div>
		
		<div class="clblock b-abajo b-derecha"><img src="images/cl-tororey.png" alt="cl-tororey" width="160" height="39" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-daonsa.png" alt="cl-daonsa" width="140" height="57" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-cemix.png" alt="cl-cemix" width="178" height="65" /></div>
		<div class="clblock b-abajo"><img src="images/cl-cadtoner.png" alt="cl-cadtoner" width="140" height="74" /></div>
		
		<div class="clblock b-derecha"><img src="images/cl-chen.png" alt="cl-chen" width="140" height="140" /></div>
		<div class="clblock b-derecha"><img src="images/cl-juliocepeda.png" alt="cl-juliocepeda" width="140" height="140" /></div>
		<div class="clblock b-derecha"><img src="images/cl-frisa.png" alt="cl-frisa" width="85" height="114" /></div>
		<div class="clblock b-ultimo"><img src="images/cl-berel.png" alt="cl-berel" width="140" height="140" /></div>
	</div> 
</section>


<?php include('footer.php'); ?>

<script src="js/jquery.cycle.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/center.js"></script>
<script src="js/empresa.js"></script>
<script>
	$(document).ready(function() {
		$('#l1').addClass("activadote");
	});
</script>
</body>
</html>