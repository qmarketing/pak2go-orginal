<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<section class="empresa">
	<div class="empresaNav">
		<a href="#" class="clickQuienes">About us</a>
		<a href="#" class="clickMVV">Mission, visions and values</a>
		<a href="#" class="clickHistoria">History</a>
		<a href="#" class="clickClientes">Clients</a>
	</div>
	
	<div class="empresaFrase">
		<h2><span>{</span>Nice to meet you<span>}</span></h2>
	</div>
	
	<div class="quienesSomos">
		<article class="info clearfix">
			<h3><img src="images/quote1.png"/>PAK2GO LOGISTICS was born by the vision of offering logistic solutions, creating a fan of options with which they are covering 2 main objectives: Customers’ satisfaction and the affordability of the companies that we work with.<img src="images/quote2.png"/></h3>
			<div class="masInfo"><p>In Pak2Go Logistics we know that each of our customers has different needs, that is why we are always looking for the best options so we can create a suite that can satisfy all the needs of everyone.</p></div>
			<div class="socio">
				<h4>Your Logistics partner</h4>
				<p>We offer the best solution in warehouse, distribution and logistics. We are more than a solution, we are a real logistics partner for your company.</p>
			</div>
			<div class="objetivo">
				<h4>Objective</h4>
				<p>Our main objective is to make our customers focus on how they can increase their sales while they leave the logistics on expert hands.</p>
			</div>
		</article>
	</div>
	
	<div class="mvv">
		<article class="info clearfix">
			<div class="sencillo mvvblock"><h3>Simple</h3></div>
			<div class="economico mvvblock"><h3>Economic</h3></div>
			<div class="rapido mvvblock"><h3>Rapid</h3></div>
		</article>
		<article class="info2 clearfix">
			<div class="mision">
				<h4>Mission</h4>
				<p>Convert the logistics department of our customers on something SER simple, economic and rapid.</p>
			</div>
			
			<div class="vision">
				<h4>Vision</h4>
				<p>Provide logistics solutions SER by an international level.</p>
			</div>
		</article>
	</div>
	
	<div class="history">
		<div class="info clearfix">
			<span class="titulo"><img src="images/i-historia.png" alt="Historia"/></span>
			<a href="#" class="hprev"></a>
			<a href="#" class="hnext"></a>
			<div class="hslide">
				<div class="hblock clearfix">
					<span class="fecha">2008</span>
					<div class="htxt h2008">
						<p>
							We start operations in a small office in Monterrey, México.<br><br>
							Personnel: 3<br>
							Customers: 30<br>
							Offices: 1
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2009</span>
					<div class="htxt h2009">
						<p>
							Guadalajara’s office starts operations.<br>
							In Monterrey we move to a warehouse offering storage services.<br><br>
							Personnel: 10<br>
							Customers: 260<br>
							Offices: 2
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2010</span>
					<div class="htxt h2010">
						<p>
							Opening of three more offices. Chihuahua, Reynosa and León.<br><br>
							In Guadalajara we move to a warehouse offering storage services.<br><br>
							Personnel: 25<br>
							Customers: 520<br>
							Offices: 5
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2011</span>
					<div class="htxt h2011">
						<p>
							Opening in México, DF.<br>
							In Chihuahua we move to a warehouse offering storage services.<br><br>
							Personnel: 45<br>
							Customers: 900<br>
							Offices: 6
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2012</span>
					<div class="htxt h2012">
						<p>
							Opening of Pak2Go International – Starts coverage worldwide.<br>
							Opening in Veracruz.<br>
							In México, DF we move to a warehouse offering storage services.<br><br>
							Personnel: 75<br>
							Customers: 1500<br>
							Offices: 7
						</p>
					</div>
				</div>
				
				<div class="hblock clearfix">
					<span class="fecha">2013</span>
					<div class="htxt h2013">
						<p>
							Opening in Hong Kong Pak2Go Limited.<br><br>
							Personnel : 120<br>
							Customers: 2,000<br>
							Offices: 10
						</p>
					</div>
				</div>
			</div>
		</div>

		<div class="greatplace"><img class="logo2" src="../images/greatplace.jpg" alt=""></div>
	</div>
	
	<div class="clientes clearfix">
		<div class="clblock b-abajo b-derecha"><img src="images/cl-aoc.png" alt="cl-aoc" width="138" height="46" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-cityclub.png" alt="cl-cityclub" width="98" height="99" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-totalchef.png" alt="cl-totalchef" width="130" height="107" /></div>
		<div class="clblock b-abajo"><img src="images/cl-kenworth.png" alt="cl-kenworth" width="178" height="30" /></div>
		
		<div class="clblock b-abajo b-derecha"><img src="images/cl-photofolio.png" alt="cl-photofolio" width="154" height="34" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-sigma.png" alt="cl-sigma" width="145" height="83" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-soriana.png" alt="cl-soriana" width="176" height="48" /></div>
		<div class="clblock b-abajo"><img src="images/cl-tecdemty.png" alt="cl-tecdemty" width="175" height="64" /></div>
		
		<div class="clblock b-abajo b-derecha"><img src="images/cl-tororey.png" alt="cl-tororey" width="160" height="39" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-daonsa.png" alt="cl-daonsa" width="140" height="57" /></div>
		<div class="clblock b-abajo b-derecha"><img src="images/cl-cemix.png" alt="cl-cemix" width="178" height="65" /></div>
		<div class="clblock b-abajo"><img src="images/cl-cadtoner.png" alt="cl-cadtoner" width="140" height="74" /></div>
		
		<div class="clblock b-derecha"><img src="images/cl-chen.png" alt="cl-chen" width="140" height="140" /></div>
		<div class="clblock b-derecha"><img src="images/cl-juliocepeda.png" alt="cl-juliocepeda" width="140" height="140" /></div>
		<div class="clblock b-derecha"><img src="images/cl-frisa.png" alt="cl-frisa" width="85" height="114" /></div>
		<div class="clblock b-ultimo"><img src="images/cl-berel.png" alt="cl-berel" width="140" height="140" /></div>
	</div>
</section>

<?php include('footer.php'); ?>

<script src="js/jquery.cycle.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/center.js"></script>
<script src="js/empresa.js"></script>
<script>
	$(document).ready(function() {
		$('#l1').addClass("activadote");
	});
</script>
</body>
</html>