<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<div class="sNacBanner">
	<h1>Domestic<br>Services</h1>
	<a href="#"></a>
</div>

<section class="fdInfo">
	<div class="conts clearfix">
		<h2>Freight and <br>Distribution</h2>
		<div class="more">
			<h3>Local, national and international distribution of your merchandise through the following options:</h3>
			<ul>
				<li>Local, national and international parcel, land and air</li>
				<li>LTL to everywhere inside México</li>
				<li>Local and national Direct Shipping (1.5 tons. 3.5 tons and dry vans 48´and 53´)</li>
				<li>We personally monitor your shipments, a person is assigned to your account and responsible for your complete satisfaction (no extra cost)</li>
				<li>We have strategic alliances with companies of great prestige and quality</li>
			</ul>
		</div>
		<div class="figFD">
			<img src="images/fd1.png" class="fd1" width="135" height="150" />
			<img src="images/fd2.png" class="fd2" width="249" height="278" />
			<img src="images/fd3.png" class="fd3" width="348" height="388" />
			<img src="images/fd4.png" class="fd4" width="416" height="464" />
			<img src="images/fd5.png" class="fd5" width="492" height="548" />
			<img src="images/fd6.png" class="fd6" width="566" height="632" />
			<img src="images/fd7.png" class="fd7" width="626" height="698" />
		</div>
	</div>
</section>

<section class="almacenaje">
	<div class="linea1"></div>
	<div class="conts clearfix">
		<h2>Storage</h2>
		<ul>
			<li>We have over 5000 m2 in the city of Monterrey</li>
			<li>Inventory storage space from 25m m2</li>
			<li>Inventory control</li>
			<li>Security system 24/7</li>
			<li>Maximum security closed circuit</li>
			<li>Loading and unloading of units</li>
			<li>Handling Services</li>
		</ul>
	</div>
</section>

<section class="pickpack">
	<div class="linea2"></div>
	<div class="conts">
		<h2>Pick & Pack</h2>
		<!-- <h3>Surtido y armado de pedidos</h3> -->
		<p>As part of our comprehensive service, staff is ready to fill and assemble orders with strict quality rules, considering processes imposed by the client. Through purchase orders we will be filling your orders and pack them ready to be shipped to wherever is needed</p>
	</div>
</section>

<section class="asesores">
	<div class="linea3"></div>
	<div class="conts clearfix">
		<article class="txt">
			<h2>Logistics <br>Consultants</h2>
			<p>We are experts in the matter, we advise you so your warehouse and distribution will be handle in the most efficient way, fast and safe. Specially with a focus on cost reduction and profitability for your company.</p>
		</article>
	</div>
	</div>
</section>

<div class="nacional">
	<div class="linea4"></div>
	
	<div class="isoNacional">
		<div class="nacionalStartC"></div>
		<div class="nacionalStart">View domestic process</div>
		
		<div class="clickMas beat" id="snpaso1">+</div>
		<div class="clickMas beat" id="snpaso2">+</div>
		<div class="clickMas beat" id="snpaso3">+</div>
		<div class="clickMas beat" id="snpaso4">+</div>
		
		<div class="isoTxt" id="snpaso1txt">
			<h2>PAK2GO</h2><p>Place an order, We can ship packages, pallets, direct local and national freights.</p>
		</div>
		<div class="isoTxt" id="snpaso2txt">
			<h2>To your company</h2><p>We take care of the transportation of your goods to our warehouse.</p>
		</div>
		<div class="isoTxt" id="snpaso3txt">
			<h2>Storage</h2><p>We store, pack and label according to your company's processes</p>
		</div>
		<div class="isoTxt" id="snpaso4txt">
			<h2>Distribution</h2><p>We deliver anywhere in the republic, and keep special tracking of all your shipments.</p>
		</div>
		
		<div class="empezarnac"></div>
		
		<div class="piso"></div>
		<img src="images/sn-avion.png" class="sn-avion" />
		<img src="images/sn-cajas.png" class="sn-caja1" />
		<img src="images/sn-cajas.png" class="sn-caja2" />
		<img src="images/sn-camion1.png" class="sn-camion1" />
		<img src="images/sn-camion3.png" class="sn-camion3" />
		<img src="images/sn-camion4.png" class="sn-camion4" />
		<img src="images/sn-camion4.png" class="sn-camion4-1" />
		<img src="images/sn-camion5.png" class="sn-camion5" />
		<img src="images/sn-casa1.png" class="sn-casa1" />
		<img src="images/sn-casa2.png" class="sn-casa2" />
		<img src="images/sn-casa2.png" class="sn-casa2-1" />
		<img src="images/sn-casa2.png" class="sn-casa2-2" />
		<img src="images/sn-casa3.png" class="sn-casa3" />
		<img src="images/sn-casa4.png" class="sn-casa4" />
		<img src="images/sn-casa5.png" class="sn-casa5" />
		<img src="images/sn-camion2.png" class="sn-camion2" />
		<img src="images/sn-flecha1.png" class="sn-flecha1" />
		<img src="images/sn-flecha2.png" class="sn-flecha2" />
		<img src="images/sn-flecha3.png" class="sn-flecha3" />
		<img src="images/sn-flecha4.png" class="sn-flecha4" />
		<img src="images/sn-grua.png" class="sn-grua" />
		<img src="images/pak2go-big.png" class="pak2gob1"/>
		<img src="images/logos-nacional.png" class="logos-nacional"/>
	</div>
	
</div>

<?php include('footer.php'); ?>

<script src="js/easing.js"></script>
<script src="js/center.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/nacionales.js"></script>
<script>
	$(document).ready(function() {
		$('#l2').addClass("activadote");
	});

</script>

</body>
</html>