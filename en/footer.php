<footer class="footerWrap">
	<div class="footer clearfix">
		<span class="footerLogo"></span>
		<div class="cont">
			<div class="telsFooter">
				<div class="icon"></div>
				01800 APOYO 06 <span>(Todo México)</span>
				(81) 1365 5333 <span>(Monterrey)</span>
				(614) 481 0497 <span>(chihuahua)</span>
				(899) 925 5342 <span>(reynosa)</span>
				<br>
				(55) 5531 3553 <span>(distrito federal)</span>
				(33) 3615 6427 <span>(guadalajara)</span>
				(477) 717 5906 <span>(león)</span>
				(229) 293 9950 <span>(veracruz)</span>
				(656) 408 0520 <span>(cd. juárez)</span>
				(444) 274 0307 <span>(San Luis Potosí)</span>
				(222) 169 6425 <span>(Puebla)</span>
				(852) 2201 1066 <span>(PAK2GO LIMITED)</span>
			</div>

			<div class="telsFooter2">
				<div class="icon"></div>
				<p>01800 APOYO 06 <span>(Todo México)</span></p>
				<p>(81) 1365 5333 <span>(Monterrey)</span></p>
				<p>(614) 481 0497 <span>(chihuahua)</span></p>
				<p>(899) 925 5342 <span>(reynosa)</span></p>
				<p>(55) 5531 3553 <span>(distrito federal)</span></p>
				<p>(33) 3615 6427 <span>(guadalajara)</span></p>
				<p>(477) 717 5906 <span>(león)</span></p>
				<p>(229) 293 9950 <span>(veracruz)</span></p>
				<p>(656) 408 0520 <span>(cd. juárez)</span></p>
				<p>(444) 274 0307 <span>(San Luis Potosí)</span></p>
				<p>(222) 169 6425 <span>(Puebla)</span></p>
				<p>(852) 2201 1066 <span>(PAK2GO LIMITED)</span></p>
			</div>

			<div class="conect">
				<div class="icon"></div>
				<a href="mailto:info@pak2go.com" class="mailLink">info@pak2go.com</a>
				<a href="http://www.facebook.com/pak2go" target="_blank"><img src="images/f-facebook.png" alt="Facaebook Pak2Go"/></a>
				<a href="http://www.twitter.com/pak2go" target="_blank"><img src="images/f-twitter.png" alt="Twitter Pak2Go"/></a>
			</div>
			<nav class="footerNav">
				<a href=".">Home</a>
				<a href="company.php">Company</a>
				<a href="domestic-services.php">Domestic Services</a>
				<a href="international-services.php">International Services</a>
				<a href="resources.php">Resources</a>
				<a href="contact.php">Contact</a>
				<a href="../">Español</a>
			</nav>
		</div>
		<div class="copy">©2015 PAK2GO</div>
	</div>
</footer>


<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
 // window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
 // d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
 // _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
 // $.src='//v2.zopim.com/?1dRtF3GyWJSwUOmopyUtZUtcxtbty1dg';z.t=+new Date;$.
 // type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
</script>
<!--End of Zopim Live Chat Script-->