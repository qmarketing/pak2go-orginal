<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Foreign Trade</title>

<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<div class="cExteriorBanner">
	<h1>Foreign<br>Trade</h1>
	<a href="#"></a>
</div>

<div class="clearfix">
<section class="asesoriaComercio">
	<article class="as1">
		<h4>STUDY</h4>
		<br/><br/>
		<span></span>
		<h3>Need to know how much put your product in your cellar in Mexico cost?  <br>If it is so, we need the following information:</h3>
		<ul>
			<li>Quote / Order (for requerimient understanding)</li>
			<li>Data sheet / Pictures  (for goods clasification)</li>
			<li>Packaging list (for Logistic loads)</li>
			<li>Contact</li>
		</ul>
	</article>
	
	<article class="as2">
		<h4>FOREIGN TRADE COORDINATION (with list of importers)</h4>
		<br/><br/>
		<span></span>
		<h3>Looking to outsource a department of Foreign Trade, custom made at your needs?<br>If it si so, we have the solution for you:</h3>
		<ul>
			<li>We contact your supplier (order programming, norm accomplishment, documentation, delivery.)</li>
			<li>We operate International Logistics (collection programming, dispatch, home delivery)</li>
			<li>We operate Import Procedure(register programming with A.A.,  documents, payments and  experienced track)</li>
			<li>We operate National Logistics (home delivery programming)</li>
			<p>Note: All the documentation on behalf of your company!</p>
		</ul>
	</article>
	
	<article class="as3">
		<h4>FOREIGN TRADE COORDINATION (without list of importers)</h4>
		<br /><br />
		<span></span>
		<h3>Looking for INTEGRAL support for your Foreign Trade operation, custom made at your needs? <br>If it si so, we have the solution for you:</h3>
		<ul>
			<li>We contact your supplier (order programming, norm accomplishment, documentation, delivery and posible negotiation)</li>
			<li>We operate International Logistics (collection programming, dispatch, home delivery)</li>
			<li>We operate Import Procedure(register programming with A.A.,  documents, payments and  experienced track)</li>
			<li>We operate National Logistics (home delivery programming)</li>
		</ul>
		<p>Nota: Factura de empresas Mexicanas con desglose de servicios.</p>
	</article>
</section>

<section class="asesoriaComercio2 ">
	<article class="as1">
		<h4>RECURRENCE IN FOREIGN TRADE (frequent customers)</h4>
		<br /><br />
		<span></span>
		<h3>Looking for support in your Foreign Trade business operation ?<br>If it si so, we have the solution for you:</h3>
		<ul>
			<li>We Collect and make documenttion (We guide the client to organice informative requeriments)</li>
			<li>We operate International Logistic (collection programming, dispatch, home delivery)</li>
			<li>We Support you with  import step (register programming with A.A.,  documents, payments notification and  experienced track)</li>
			<li>We operate National Logistics (destiny delivery programming)</li>
			<p>Note: All the documentation on behalf of your company!</p>
		</ul>
	</article>
	
	<article class="as2">
		<h4>WCA COMMERCIAL ALLIANCE</h4>
		<br /><br />
		<span></span>
		<h3>Buscas apoyo en tu operación con Mexico?<br>Si asi lo es, tenemos la solución para ti:</h3>
		<ul>
			<li>We Collect and make documentation (We organice the informative requeriments)</li>
			<li>We supervise your International Logistics (we guide the process on arrival in port;destination port expenses)</li>
			<li>We Support you with  import step (register programming with A.A.,  documents, payments notification and  experienced track)</li>
			<li>We operate National Logistics (destiny delivery programming)</li>
		</ul>
	</article>
	
	<article class="as3">
		<h4>CERTIFICATES</h4>
		<br /><br />
		<span></span>
		<h3>The fraction its classified with Certificate requisition? <br>If it is so:</h3>
		<ul>
			<li>We guide you to understand and / or process the necessary paperwork to comply with all detailed restrictions.</li>
		</ul>
	</article>
</section>
<section class="asesoriaComercio2 ">
</section>



<section class="ComercioInfo">
	<div class="cont clearfix">
	<article class="tc1 clearfix">
		<h4>ORIGIN VERIFICATION (Asia and Asian Souteast)</h4>
		<div class="all">
			<p>Looking to confirm existence of companies, quality, packaging, business scope of your supplier?</p>
			<div class="col1">
				<h5>Option 1</h5>
				<ul>
					<li>Physically checked with a visit to the company to make sure the address you gave us.</li>
					<li>Physically checked with product inspection.</li>
					<li>(Cliente DEBE PROPORCIONAR características <br/> exactas de lo  que busca que chequemos.)</li>
				</ul>
			</div>
			
			<div class="col2">
				<h5>Option 2</h5>
				<ul>
					<li>Verificamos, durante PRODUCCIÓN, que todas las especificaciones del producto y calidad se estén cumpliendo con tus expectativas.</li>
					<li>(Cliente nos DEBE PROPORCIONAR especificación exacta de lo que busca.)</li>
					<li>(Customer MUST PROVIDE accurate specifications of what we are looking for.)</li>
				</ul>
			</div>
			
			<div class="col3">
				<h5>Option 3</h5>
				<ul>
					<li>We verify the final production and quality. </li>
					<li>Estar en el momento que se esta cargando tu contenedor, del cierre y sello de salida a embarque.</li>
					<li>Being in the moment your container is loading, the closure and shipment stamp .</li>
					<li>(Customer MUST PROVIDE exact specification packaging, quality, color)</li>
				</ul>
			</div>
		</div>
			
	</article>
	</div>
</section>
</div>

<?php include('footer.php'); ?>

<script src="js/easing.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/exterior.js"></script>
</body>
</html>