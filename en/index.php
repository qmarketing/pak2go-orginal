<?php include('top.php'); ?>
<link rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/reveal2.css">

</head>
<body>
<?php include('header.php'); ?>

<div class="homeslides">
	<div class="frase"><h1><span>{</span>Your shipment is our commitment<span>}</span></h1></div>
	<div class="flexslider">
		<ul class="slides">
			<li>
				<img src="images/banner1.jpg" />
				<div class="flexCaption">
					<h1>- Your Logistic Partner -</h1><h2>We offer the best distribution <br>and warehousing solutions</h2>
				</div>
			</li>
			<li>
				<img src="images/banner2.jpg" />
				<div class="flexCaption">
					<h1>- Local, National and International Courier Services -</h1>
					<h2>We take care of your order and also <br>we can distribute it to any place in Mexico <br>trought the best logistics companies.</h2>
				</div>
			</li>
			<li>
				<img src="images/banner3.jpg" />
				<div class="flexCaption">
					<h1>- FREIGHT -</h1>
					<h2>By Land, Air & Sea. National and International</h2>
				</div>
			</li>
			<li>
				<img src="images/banner4.jpg" />
				<div class="flexCaption">
					<h1>- International Service -</h1>
					<h2>Local contact, global solution</h2>
				</div>
			</li>
		</ul>
	</div>
	<!-- <div class="slideFlechita"><a href="#"></a></div> -->
</div>

<!-- pop -->
<a href="#" class="clickPop" data-reveal-id="pop">popup</a>
<div id="pop" class="reveal-modal xlarge">
	<div>
		<img src="images/fletes-small.jpg" alt="fletes-small" width="800" height="600" />
		<a href="contact.php" title="Contactanos"></a>
	</div>
	<a class="close-reveal-modal"><span>Saltar intro</span>&#215;</a>
</div>
<!-- pop -->


<a href="domestic-services.php">
<div class="bigNacionales">
	<div class="cont clearfix">
		<h2>Domestic<br>Services</h2>
		<figure class="fig1"></figure>
		<figure class="fig2"></figure>
		<!-- <h3>Distribución de envío</h3> -->
	</div>
</div>
</a>
<a href="international-services.php">
<div class="bigInternacionales">
	<div class="cont clearfix">
		<h2>International<br>Services</h2>
		<figure class="fig1"></figure>
		<figure class="fig2"></figure>
		<!-- <h3>Asesoría internacional</h3> -->
	</div>
</div>
</a>

<div class="dostres">
<div class="trespics clearfix">
	<div class="tresBlock bloque1">
		<div>We offer <br>warehouse service with land distribution and service next day. (we send envelopes, boxes, pallets, etc.)</div><span></span>
	</div>
	
	<div class="tresBlock bloque2">
		<div>We totally control your warehouse: inventory, personnel outsourcing, pick&pack services.</div><span></span>
	</div>
	
	<div class="tresBlock bloque3">
		<div>We offer you a personalized customer service agent, he will track your shipments and (advise you about your logistics needs)help you with your logistics needs.</div><span></span>
	</div>
</div>

<div class="dospics clearfix">
	<div class="dosBlock bloque4">
		<div>We offer you logistics advisory for any logistic service you may need.</div><span></span>
	</div>
	<div class="dosBlock bloque5">
		<div>We collect, <br>local, national <br>and international.</div><span></span>
	</div>
</div>
<div class="fotoTodos"></div>
</div>
<div class="greatplace">
	<img class="greatlogo" src="images/greatplace.jpg" alt="great-place-to-work"  />
</div>



<?php include('footer.php'); ?>

<script src="js/jquery.reveal.js"></script>
<script src="js/jquery.flexslider.js"></script>
<!-- <script src="js/jquery.scrollTo.js"></script> -->
<script src="js/home.js"></script>

</body>
</html>