<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<div class="sInterBanner">
	<h1>International<br>Services</h1>
	<a href="#"></a>
</div>

<section class="asesoriaInter clearfix">
	<span class="asesoriaAnchor"></span>
	<h2><span>{</span>International advisory<span>}</span></h2>
	<article class="as1">
		<span></span>
		<h3>Not sure where to start? <br> Need help with the import?</h3>
		<ul>
			<li>We give you advice in the process of import export</li>
			<li>We help you get that international business</li>
			<li>We analyze the the viability of import / export your products</li>
		</ul>
	</article>
	
	<article class="as2">
		<span></span>
		<h3>Confused with the load? <br>Do i need a 20' or 40' container? <br>Can i insure my merchandise? <br>Is it better by seafreight or airfreight?</h3>
		<ul>
			<li>We assemble your shipment and insure your goods</li>
			<li>We coordinate your shipments at origin and destination</li>
			<li>We take care of all the planning</li>
		</ul>
	</article>
	
	<article class="as3">
		<span></span>
		<h3>Do you constantly lose your shipment visibility? <br>Did you use the best route for your import / export?</h3>
		<ul>
			<li>We give you advice analyzing the best route to reduce costs</li>
			<li>With our technology we give you clarity and certanty during the whole process</li>
		</ul>
	</article>
</section>

<section class="transpInfo">
	<div class="cont clearfix">
		<article class="ti1 clearfix">
			<span></span>
			<h4>Air freight services</h4>
			<div class="col1">
				<h5>Express</h5>
				<ul>
					<li>We transport your package or pallet instantly, 1 day transit time.</li>
				</ul>
			</div>
			
			<div class="col2">
				<h5>Priority</h5>
				<ul>
					<li>Ideal for critical loads.</li>
				</ul>
			</div>
			
			<div class="col3">
				<h5>Standard</h5>
				<ul>
					<li>For those important shipments, but no so criticals. 5 to 6 days transit time.</li>
				</ul>
			</div>
		</article>
		
		<article class="ti2 clearfix">
			<h4>Land freight services</h4>
			<div class="col1">
				<h5>Truck load services</h5>
				<ul>
					<li>We coordinate your shipments through México, United States and Canada.</li>
					<li>Full truck load (FTL)</li>
					<li>Less than truck load (LTL)</li>
				</ul>
			</div>
			
			<div class="col2">
				<h5>Special services</h5>
				<ul>
					<li>We handle pharmaceuticals, bulk, chemicals, electronics, high value, hazardous.</li>
				</ul>
			</div>
			
			<div class="col3">
				<h5>Equipment</h5>
				<ul>
					<li>Dry van</li>
					<li>Reefer</li>
					<li>Flat bed</li>
				</ul>
			</div>			
		</article>
		
		<article class="ti3 clearfix">
			<h4>Sea freight services</h4>
			<div class="col1">
				<h5>FCL services (Full Container Load)</h5>
				<ul>
					<li>We coordinate the pickup at origin.</li>
					<li>Seafreight service per container.</li>
				</ul>
			</div>
			<div class="col2">
				<h5>LCL services (Less than Container Load)</h5>
				<ul>
					<li>We coordinate the pickup at origin and we consolidate</li>
					<li>Service starting at 1 pallet.</li>
				</ul>
			</div>
			<div class="col3">
				<h5>Special services<br>&nbsp;</h5>
				<ul>
					<li>Over dimensions, special projects, heavy weight, bulk.</li>
				</ul>
			</div>
		</article>
	</div>
</section>

<div class="internacional">
	<div class="internacionalStartC"></div>
	<div class="internacionalStart">international <br>process</div>
		
	<div class="clickMas beat" id="sipaso1">+</div>
	<div class="clickMas beat" id="sipaso2">+</div>
	<div class="clickMas beat" id="sipaso3">+</div>
	<div class="clickMas beat" id="sipaso4">+</div>
	<div class="clickMas beat" id="sipaso5">+</div>
	<div class="clickMas beat" id="sipaso6">+</div>
	<div class="clickMas beat" id="sipaso7">+</div>
	<div class="clickMas beat" id="sipaso8">+</div>
	
	<div class="isoTxt" id="sipaso1txt"><h2>Pickup at origin</h2><p>We collect your merchandise at your factory or warehouse.</p></div>
<!-- 	<div class="isoTxt" id="sipaso2txt"><h2>Tu empresa</h2><p>Nos hacemos cargo de la transportación de tu mercancía a nuestros almacenes.</p></div> -->
	<div class="isoTxt" id="sipaso3txt"><h2>Merchandise loading at origin</h2><p>Load of full container LCL.</p></div>	
	<div class="isoTxt" id="sipaso4txt"><h2>Customs at origin</h2><p>Exportation customs clearance.</div>
	<div class="isoTxt" id="sipaso5txt"><h2>International main freight</h2></div>
	<div class="isoTxt" id="sipaso6txt"><h2>Customs at origin</h2><p>Importation customs clearance.</p></div>
	<div class="isoTxt" id="sipaso7txt"><h2>Unloading goods <br>at destination</h2></div>
	<div class="isoTxt" id="sipaso8txt"><h2>Destination delivery</h2></div>
	
	<div class="empezarinter"></div>
	
	<div class="piso"></div>
	<img src="images/pak2go-big.png" class="pak2gob2" width="440" height="73" />
	<img src="images/si-avion.png" class="si-avion" width="198" height="135" />
	<img src="images/si-caja.png" class="si-caja2" width="44" height="41" />
	<img src="images/si-caja.png" class="si-caja1" width="44" height="41" />
	<img src="images/si-caja.png" class="si-caja4" width="44" height="41" />
	<img src="images/si-caja.png" class="si-caja3" width="44" height="41" />
	<img src="images/si-camion1.png" class="si-camion1" width="38" height="38" />
	<img src="images/si-camion1.png" class="si-camion2" width="38" height="38" />
	<img src="images/si-camion1.png" class="si-camion3" width="38" height="38" />
	
	<img src="images/si-camion3.png" class="si-camion5" width="35" height="39" />
	<img src="images/si-camionblanco1.png" class="si-camionblanco1" width="51" height="49" />
	<img src="images/si-camionblanco1.png" class="si-camionblanco2" width="51" height="49" />
	<img src="images/si-camionblanco2.png" class="si-camionblanco3" width="50" height="47" />
	<img src="images/si-camionblanco2.png" class="si-camionblanco4" width="50" height="47" />
	<img src="images/si-casa1.png" class="si-casa1" width="59" height="80" />
	<img src="images/si-casa2.png" class="si-casa2-1" width="51" height="45" />
	<img src="images/si-casa2.png" class="si-casa2-2" width="51" height="45" />
	<img src="images/si-casa2.png" class="si-casa2-3" width="51" height="45" />
	<img src="images/si-casa2.png" class="si-casa2-4" width="51" height="45" />
	<img src="images/si-casa3.png" class="si-casa3" width="145" height="97" />
	<img src="images/si-casa4.png" class="si-casa4" width="75" height="181" />
	<img src="images/si-casa5.png" class="si-casa5" width="127" height="98" />
	<img src="images/si-casa5add.png" class="si-casa5add" width="86" height="89" />
	<img src="images/si-casa6.png" class="si-casa6" width="112" height="86" />
	<img src="images/si-casa7.png" class="si-casa7" width="119" height="92" />
	<img src="images/si-camion2.png" class="si-camion4" width="35" height="39" />
	<img src="images/si-flecha1.png" class="si-flecha1" width="26" height="17" />
	<img src="images/si-flecha2.png" class="si-flecha2" width="23" height="17" />
	<img src="images/si-flecha3.png" class="si-flecha3" width="27" height="19" />
	<img src="images/si-flecha2.png" class="si-flecha4" width="23" height="17" />
	<img src="images/si-flecha3.png" class="si-flecha5" width="27" height="19" />
	<img src="images/si-flecha2.png" class="si-flecha6" width="23" height="17" />
	<img src="images/si-grua1.png" class="si-grua1" width="22" height="22" />
	<img src="images/si-grua1.png" class="si-grua2" width="22" height="22" />
	<img src="images/si-tarimas.png" class="si-tarima1" width="42" height="32" />
	<img src="images/si-tarimas.png" class="si-tarima2" width="42" height="32" />
	<img src="images/logos-int.png" class="logos-int" width="233" height="107" />
</div>

<?php include('footer.php'); ?>

<script src="js/easing.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/inter.js"></script>
<script>
	$(document).ready(function() {
		$('#l3').addClass("activadote");
	});
</script>
</body>
</html>