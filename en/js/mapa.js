var styles = [
	{
		featureType: 'water',
		elementType: 'all',
		stylers: [
			{ hue: '#646464' },
			{ saturation: -100 },
			{ lightness: -48 },
			{ visibility: 'on' }
		]
	},{
		featureType: 'landscape',
		elementType: 'all',
		stylers: [
			{ hue: '#3e3e3e' },
			{ saturation: -100 },
			{ lightness: -73 },
			{ visibility: 'on' }
		]
	},{
		featureType: 'landscape.man_made',
		elementType: 'all',
		stylers: [
			{ hue: '#848484' },
			{ saturation: -100 },
			{ lightness: -42 },
			{ visibility: 'on' }
		]
	},{
		featureType: 'road',
		elementType: 'all',
		stylers: [
			{ hue: '#3d3d3d' },
			{ saturation: -100 },
			{ lightness: -63 },
			{ visibility: 'on' }
		]
	},{
		featureType: 'road.highway',
		elementType: 'all',
		stylers: [
			{ hue: '#b6b6b6' },
			{ saturation: -100 },
			{ lightness: 20 },
			{ visibility: 'on' }
		]
	},{
		featureType: 'road.arterial',
		elementType: 'all',
		stylers: [
			{ hue: '#b6b6b6' },
			{ saturation: -100 },
			{ lightness: -7 },
			{ visibility: 'on' }
		]
	},{
		featureType: 'road.local',
		elementType: 'all',
		stylers: [
			{ hue: '#b6b6b6' },
			{ saturation: -100 },
			{ lightness: -29 },
			{ visibility: 'on' }
		]
	},{
		featureType: 'poi.park',
		elementType: 'all',
		stylers: [
			{ hue: '#646464' },
			{ saturation: -100 },
			{ lightness: -50 },
			{ visibility: 'on' }
		]
	},{
		featureType: 'poi',
		elementType: 'all',
		stylers: [
			{ hue: '#646464' },
			{ saturation: -100 },
			{ lightness: -50 },
			{ visibility: 'on' }
		]
	},{
		featureType: 'water',
		elementType: 'all',
		stylers: [

		]
	}
];

var options = {
	mapTypeControlOptions: {
		mapTypeIds: [ 'Styled']
	},
	center: new google.maps.LatLng(25.666899557429442, -100.37339797019956),
	zoom: 4,
	mapTypeId: 'Styled'
};
function mappak2go() {
    var latlng = new google.maps.LatLng(25.669468,-100.418985);
    var myOptions = {
      zoom: 4,
      scaleControl: false,
      scrollwheel: false,
      center: latlng,
      mapTypeControlOptions: {
		mapTypeIds: [ 'map_canvas']
		},
      mapTypeId: 'map_canvas',
	   
	  mapTypeControl: true,
   	  mapTypeControlOptions: {
        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
        position: google.maps.ControlPosition.TOP_RIGHT,
    },
	  
    };
    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
	var styledMapType = new google.maps.StyledMapType(styles, { name: 'map_canvas' });
	map.mapTypes.set('map_canvas', styledMapType)
	
	
	var content_mty = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Pak2Go Monterrey</h1>'+
        '<div id="bodyContent">'+
        '<p>Argentina 205-1<br>Col. Parque Industrial Martel, Santa Catarina, Nuevo León CP: 66367</p>' +
        '</div>'+
        '</div>';
    var infowindow_mty = new google.maps.InfoWindow({
        content: content_mty,
		maxWidth: 270
    });
    var marker_mty = new google.maps.Marker({
        position: new google.maps.LatLng (25.755638,-100.188156),
        map: map,
        icon:"images/markericon.png",
        title: 'Pak2Go Monterrey'
    });
    google.maps.event.addListener(marker_mty, 'click', function() {
      infowindow_mty.open(map,marker_mty);
    });
    
    var content_chihua = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Pak2Go Chihuahua</h1>'+
        '<div id="bodyContent">'+
        '<p>Paseo de Aldama 13906 <br>Col. Paseo de Chihuahua CP 31205 <br>Chihuahua, Chihuahua, México</p>' +
        '</div>'+
        '</div>';
    var infowindow_chihua = new google.maps.InfoWindow({
        content: content_chihua,
		maxWidth: 270
    });
    var marker_chihua = new google.maps.Marker({
        position: new google.maps.LatLng (28.63060,-106.0737),
        map: map,
        icon:"images/markericon.png",
        title: 'Pak2Go Chihuahua'
    });
    google.maps.event.addListener(marker_chihua, 'click', function() {
      infowindow_chihua.open(map,marker_chihua);
    });
    
    var content_reyn = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Pak2Go Reynosa</h1>'+
        '<div id="bodyContent">'+
        '<p>5 de Mayo 205C <br>Col. Bellavista entre Laredo y Veracruz <br>Reynosa, Tamps., México</p>' +
        '</div>'+
        '</div>';
    var infowindow_reyn = new google.maps.InfoWindow({
        content: content_reyn,
		maxWidth: 270
    });
    var marker_reyn = new google.maps.Marker({
        position: new google.maps.LatLng (26.08393,-98.2878),
        map: map,
        icon:"images/markericon.png",
        title: 'Pak2Go Reynosa'
    });
    google.maps.event.addListener(marker_reyn, 'click', function() {
      infowindow_reyn.open(map,marker_reyn);
    });
    
    var content_df = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Pak2Go Distrito Federal</h1>'+
        '<div id="bodyContent">'+
        '<p>Calle del Rio # 8<br>Fracc. Industrial Alce Blanco, Naucalpan, Estado de México</p>' +
        '</div>'+
        '</div>';
    var infowindow_df = new google.maps.InfoWindow({
        content: content_df,
		maxWidth: 270
    });
    var marker_df = new google.maps.Marker({
        position: new google.maps.LatLng (19.480651, -99.228004),
        map: map,
        icon:"images/markericon.png",
        title: 'Pak2Go Distrito Federal'
    });
    google.maps.event.addListener(marker_df, 'click', function() {
      infowindow_df.open(map,marker_df);
    });
    
    var content_gdl = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Pak2Go Guadalajara</h1>'+
        '<div id="bodyContent">'+
        '<p>Pino Suarez #1039 bodega 25 <br>Col. El Vigía, CP 45145 <br>Zapopan, Jalisco</p>' +
        '</div>'+
        '</div>';
    var infowindow_gdl = new google.maps.InfoWindow({
        content: content_gdl,
		maxWidth: 270
    });
    var marker_gdl = new google.maps.Marker({
        position: new google.maps.LatLng (20.7362,-103.3885),
        map: map,
        icon:"images/markericon.png",
        title: 'Pak2Go Guadalajara'
    });
    google.maps.event.addListener(marker_gdl, 'click', function() {
      infowindow_gdl.open(map,marker_gdl);
    });
    
    var content_leon = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Pak2Go León</h1>'+
        '<div id="bodyContent">'+
        '<p>Paseo del Moral #520-A int.4 <br>Col. Paseo del Moral, CP 37160 <br>León, Guanajuato</p>' +
        '</div>'+
        '</div>';
    var infowindow_leon = new google.maps.InfoWindow({
        content: content_leon,
		maxWidth: 270
    });
    var marker_leon = new google.maps.Marker({
        position: new google.maps.LatLng (21.1503,-101.6899),
        map: map,
        icon:"images/markericon.png",
        title: 'Pak2Go León'
    });
    google.maps.event.addListener(marker_leon, 'click', function() {
      infowindow_leon.open(map,marker_leon);
    });
    
    var content_cordoba = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Pak2Go Córdoba</h1>'+
        '<div id="bodyContent">'+
        '<p>Ave. 3 #107 <br>Col. Centro, CP 94470 <br>Fortin, Veracruz</p>' +
        '</div>'+
        '</div>';
    var infowindow_cordoba = new google.maps.InfoWindow({
        content: content_cordoba,
		maxWidth: 270
    });
    var marker_cordoba = new google.maps.Marker({
        position: new google.maps.LatLng (18.8989,-96.9994),
        map: map,
        icon:"images/markericon.png",
        title: 'Pak2Go Córdoba'
    });
    google.maps.event.addListener(marker_cordoba, 'click', function() {
      infowindow_cordoba.open(map,marker_cordoba);
    });
    
    var content_juarez = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Pak2Go Cd. Juárez</h1>'+
        '<div id="bodyContent">'+
        '<p>Calle Vicente Guerrero #5723 <br> Col. Alamos de San Lorenzo, CP 32340, Cd. Juárez, Chihuahua</p>' +
        '</div>'+
        '</div>';
    var infowindow_juarez = new google.maps.InfoWindow({
        content: content_juarez,
		maxWidth: 270
    });
    var marker_juarez = new google.maps.Marker({
        position: new google.maps.LatLng (31.73324,-106.44005),
        map: map,
        icon:"images/markericon.png",
        title: 'Pak2Go Cd. Juárez'
    });
    google.maps.event.addListener(marker_juarez, 'click', function() {
      infowindow_juarez.open(map,marker_juarez);
    });
    
    var content_limited = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Pak2Go Limited</h1>'+
        '<div id="bodyContent">'+
        '<p>Unit 702, 7/F, Bangkok Bank Building<br>No. 18 Bonham Strand West, Hong Kong</p>' +
        '</div>'+
        '</div>';
    var infowindow_limited = new google.maps.InfoWindow({
        content: content_limited,
		maxWidth: 270
    });
    var marker_limited = new google.maps.Marker({
        position: new google.maps.LatLng (22.286655,114.148550),
        map: map,
        icon:"images/markericon.png",
        title: 'Pak2Go Limited'
    });
    google.maps.event.addListener(marker_limited, 'click', function() {
      infowindow_limited.open(map,marker_limited);
    });
    
  //   var content_chicago = '<div id="content">'+
  //       '<div id="siteNotice">'+
  //       '</div>'+
  //       '<h1 id="firstHeading" class="firstHeading">Chicago</h1>'+
  //       '<div id="bodyContent">'+
  //       '<p>5930 S. Kolmar Av.<br>Chicago, Il. 60629<p>' +
  //       '</div>'+
  //       '</div>';
  //   var infowindow_chicago = new google.maps.InfoWindow({
  //       content: content_chicago,
		// maxWidth: 270
  //   });
  //   var marker_chicago = new google.maps.Marker({
  //       position: new google.maps.LatLng (41.784832,-87.736865),
  //       map: map,
  //       icon:"images/markericon.png",
  //       title: 'Chicago'
  //   });
    // google.maps.event.addListener(marker_chicago, 'click', function() {
    //   infowindow_chicago.open(map,marker_chicago);
    // });
    
    var content_saltillo = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Saltillo</h1>'+
        '<div id="bodyContent">'+
        '<p>Av. Universidad 1380<br>Col. Valle Universidad<br>Saltillo, Coahuila C.P. 25260<p>' +
        '</div>'+
        '</div>';
    var infowindow_saltillo = new google.maps.InfoWindow({
        content: content_saltillo,
		maxWidth: 270
    });
    var marker_saltillo = new google.maps.Marker({
        position: new google.maps.LatLng (25.4447285,-101.0025771),
        map: map,
        icon:"images/markericon.png",
        title: 'Saltillo'
    });
    google.maps.event.addListener(marker_saltillo, 'click', function() {
      infowindow_saltillo.open(map,marker_saltillo);
    });
    
    var content_tijuana = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Tijuana</h1>'+
        '<div id="bodyContent">'+
        '<p>Plaza Internacional Otay<br>Calle Bellas Artes 17686 Local 117A<br>Fraccionamiento Garita De Otay<br>Tijuana Baja California C.P. 22509<p>' +
        '</div>'+
        '</div>';
    var infowindow_tijuana = new google.maps.InfoWindow({
        content: content_tijuana,
		maxWidth: 270
    });
    var marker_tijuana = new google.maps.Marker({
        position: new google.maps.LatLng (32.543572, -116.937003),
        map: map,
        icon:"images/markericon.png",
        title: 'Tijuana'
    });
    google.maps.event.addListener(marker_tijuana, 'click', function() {
      infowindow_tijuana.open(map,marker_tijuana);
    });

    var content_slp = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">San Luis Potosí</h1>'+
        '<div id="bodyContent">'+
        '<p>Av. Chapultepec #1610 <br>Piso 1 Int. 3<br>Col. Desarrollo del Pedregal<br>San Luis Potosí, SLP 78295<p>' +
        '</div>'+
        '</div>';
    var infowindow_slp = new google.maps.InfoWindow({
        content: content_slp,
        maxWidth: 270
    });
    var marker_slp = new google.maps.Marker({
        position: new google.maps.LatLng (22.144911, -101.024021),
        map: map,
        icon:"images/markericon.png",
        title: 'San Luis Potosí'
    });
    google.maps.event.addListener(marker_slp, 'click', function() {
      infowindow_slp.open(map,marker_slp);
    });

    var content_laredo = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Laredo</h1>'+
        '<div id="bodyContent">'+
        '<p>6553 Star Court<br>Laredo, TX, 78041<p>' +
        '</div>'+
        '</div>';
    var infowindow_laredo = new google.maps.InfoWindow({
        content: content_laredo,
        maxWidth: 270
    });
    var marker_laredo = new google.maps.Marker({
        position: new google.maps.LatLng (27.553972, -99.493467),
        map: map,
        icon:"images/markericon.png",
        title: 'Laredo'
    });
    google.maps.event.addListener(marker_laredo, 'click', function() {
      infowindow_laredo.open(map,marker_laredo);
    });

    var content_mexi = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Mexicali</h1>'+
        '<div id="bodyContent">'+
        '<p>Calzada Hector Teran Teran 2382 <br>Col. Xochimilco Local 5 <br>Mexicali, Baja California. CP 21380<p>' +
        '</div>'+
        '</div>';
    var infowindow_mexi = new google.maps.InfoWindow({
        content: content_mexi,
        maxWidth: 270
    });
    var marker_mexi = new google.maps.Marker({
        position: new google.maps.LatLng (32.6066145,-115.4653352),
        map: map,
        icon:"images/markericon.png",
        title: 'Mexicali'
    });
    google.maps.event.addListener(marker_mexi, 'click', function() {
      infowindow_mexi.open(map,marker_mexi);
    });

    var content_pue = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Puebla</h1>'+
        '<div id="bodyContent">'+
        '<p>Calzada Zavaleta No. 3913 2A  <br>Col. Santa Cruz Buenavista,  <br>Puebla, Pue. C.P. 72154<p>' +
        '</div>'+
        '</div>';
    var infowindow_pue = new google.maps.InfoWindow({
        content: content_pue,
        maxWidth: 270
    });
    var marker_pue = new google.maps.Marker({
        position: new google.maps.LatLng (18.8924176,-100.3141315,2161786),
        map: map,
        icon:"images/markericon.png",
        title: 'Puebla'
    });
    google.maps.event.addListener(marker_pue, 'click', function() {
      infowindow_pue.open(map,marker_pue);
    });

var content_culi = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Culiacán</h1>'+
        '<div id="bodyContent">'+
        '<p>Calle Lesa 3283<br>Fraccionamiento Stanza Toscana  <br>Culiacán Sinaloa C.P. 80050<p>' +
        '</div>'+
        '</div>';
    var infowindow_culi = new google.maps.InfoWindow({
        content: content_culi,
        maxWidth: 270
    });
    var marker_culi = new google.maps.Marker({
        position: new google.maps.LatLng (24.8100292,-107.4573569),
        map: map,
        icon:"images/markericon.png",
        title: 'Culiacán'
    });
    google.maps.event.addListener(marker_culi, 'click', function() {
      infowindow_culi.open(map,marker_culi);
    });

var content_manz = '<div id="content">'+
        '<div id="siteNotice">'+
        '</div>'+
        '<h1 id="firstHeading" class="firstHeading">Manzanillo</h1>'+
        '<div id="bodyContent">'+
        '<p>Carr. libre a Colima km. 6.5 <br>Col. 5 de Mayo,<br>Manzanillo, Colima C.P. 28800,<p>' +
        '</div>'+
        '</div>';
    var infowindow_manz = new google.maps.InfoWindow({
        content: content_manz,
        maxWidth: 270
    });
    var marker_manz = new google.maps.Marker({
        position: new google.maps.LatLng (19.0504985,-104.315666),
        map: map,
        icon:"images/markericon.png",
        title: 'Culiacán'
    });
    google.maps.event.addListener(marker_manz, 'click', function() {
      infowindow_manz.open(map,marker_manz);
    });

}

$(document).ready(function() {
	mappak2go();
});