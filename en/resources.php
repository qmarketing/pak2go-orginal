<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Fletes Baratos en Monterrey</title>

<?php include('top.php'); ?>
<link rel="stylesheet" href="css/reveal.css">

</head>
<body>
<?php include('header.php'); ?>

<section class="recursos clearfix">
	<a href="#" class="btnMedidas" data-reveal-id="medidas">
		<div></div>
		Unit Converter
	</a>
	
	<a href="#" class="btnMoneda" data-reveal-id="moneda">
		<div></div>
		Currency Converter
	</a>
	
	<a href="descargas/incoterms.pdf" target="_blank" class="btnIncoterms">
		<div></div>
		Incoterms
	</a>
	
	<a href="#" class="btnZona" data-reveal-id="tiempo">
		<div></div>
		World Time Zone
	</a>
</section>

<?php include('footer.php'); ?>

<script src="js/jquery.reveal.js"></script>
<script src="js/jquery.transit.min.js"></script>
<script src="js/recursos.js"></script>

<div id="medidas" class="reveal-modal large">
	<h3>Unit Converter</h3>
<!-- LENGTH CONVERTER START -->

<form style="padding:0;margin:0" name="cat=Length">
<table align=center width=100% style="font-family:Arial;font-size:100%" border=0 cellpadding=0 cellspacing=0>
	<tr>
		<td style="padding:1.2ex 1.5ex;width:50%;font-family:'Bebas';font-size:18px">From:<br><input style="width:100%;font-family:Arial;font-size:100%" type=text name="bindid=left;base=1"></td>
		<td style="padding:1.2ex 1.5ex;width:50%;font-family:'Bebas';font-size:18px">To:<br><input style="width:100%;font-family:Arial;font-size:100%" type=text name="bindid=right"></td>
	</tr>
	<tr><td style="padding:1.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px">
		<select name=left size=1 style="width:100%;font-family:Arial;font-size:100%"><option value="meter" selected>meter [m]</option><option value="exameter">exameter [Em]</option><option value="petameter">petameter [Pm]</option><option value="terameter">terameter [Tm]</option><option value="gigameter">gigameter [Gm]</option><option value="megameter">megameter [Mm]</option><option value="kilometer">kilometer [km]</option><option value="hectometer">hectometer [hm]</option><option value="dekameter">dekameter [dam]</option><option value="decimeter">decimeter [dm]</option><option value="centimeter">centimeter [cm]</option><option value="millimeter">millimeter [mm]</option><option value="micrometer">micrometer [&#181;m]</option><option value="micron">micron [&#181;]</option><option value="nanometer">nanometer [nm]</option><option value="picometer">picometer [pm]</option><option value="femtometer">femtometer [fm]</option><option value="attometer">attometer [am]</option><option value="megaparsec">megaparsec [Mpc]</option><option value="kiloparsec">kiloparsec [kpc]</option><option value="parsec">parsec [pc]</option><option value="light year">light year [ly]</option><option value="astronomical unit">astronomical unit [AU, UA]</option><option value="league">league [lea]</option><option value="nautical league (UK)">nautical league (UK)</option><option value="nautical league (international)">nautical league (international) [NL]</option><option value="league (statute)">league (statute) [st.league]</option><option value="mile">mile [mi, mi(Int)]</option><option value="nautical mile (UK)">nautical mile (UK) [NM (UK)]</option><option value="nautical mile (international)">nautical mile (international) [NM]</option><option value="mile (statute)">mile (statute) [mi, mi (US)]</option><option value="mile (US survey)">mile (US survey) [mi, mi (US)]</option><option value="mile (Roman)">mile (Roman)</option><option value="kiloyard">kiloyard [kyd]</option><option value="furlong">furlong [fur]</option><option value="furlong (US survey)">furlong (US survey) [fur]</option><option value="chain">chain [ch]</option><option value="chain (US survey)">chain (US survey) [ch]</option><option value="rope">rope</option><option value="rod">rod [rd]</option><option value="rod (US survey)">rod (US survey) [rd]</option><option value="perch">perch</option><option value="pole">pole</option><option value="fathom">fathom [fath]</option><option value="fathom (US survey)">fathom (US survey) [fath]</option><option value="ell">ell</option><option value="yard">yard [yd]</option><option value="foot">foot [ft]</option><option value="foot (US survey)">foot (US survey) [ft]</option><option value="link">link [li]</option><option value="link (US survey)">link (US survey) [li]</option><option value="cubit (UK)">cubit (UK)</option><option value="hand">hand</option><option value="span (cloth)">span (cloth)</option><option value="finger (cloth)">finger (cloth)</option><option value="nail (cloth)">nail (cloth)</option><option value="inch">inch [in]</option><option value="inch (US survey)">inch (US survey) [in]</option><option value="barleycorn">barleycorn</option><option value="mil">mil [mil, thou]</option><option value="microinch">microinch</option><option value="angstrom">angstrom [A]</option><option value="a.u. of length">a.u. of length [a.u., b]</option><option value="X-unit">X-unit [X]</option><option value="fermi">fermi [F, f]</option><option value="arpent">arpent</option><option value="pica">pica</option><option value="point">point</option><option value="twip">twip</option><option value="aln">aln</option><option value="famn">famn</option><option value="caliber">caliber [cl]</option><option value="centiinch">centiinch [cin]</option><option value="ken">ken</option><option value="Russian archin">Russian archin</option><option value="Roman actus">Roman actus</option><option value="vara de tarea">vara de tarea</option><option value="vara conuquera">vara conuquera</option><option value="vara castellana">vara castellana</option><option value="cubit (Greek)">cubit (Greek)</option><option value="long reed">long reed</option><option value="reed">reed</option><option value="long cubit">long cubit</option><option value="handbreadth">handbreadth</option><option value="fingerbreadth">fingerbreadth</option><option value="Planck length">Planck length</option><option value="Electron radius (classical)">Electron radius (classical)</option><option value="Bohr radius">Bohr radius [b, a.u.]</option><option value="Earth's equatorial radius">Earth's equatorial radius</option><option value="Earth's polar radius">Earth's polar radius</option><option value="Earth's distance from sun">Earth's distance from sun</option><option value="Sun's radius">Sun's radius</option></select></td><td style="padding:1.2ex 1.5ex;width:50%;font-family:Arial;font-size:13px">
<select name=right size=1 style="width:100%;font-family:Arial;font-size:100%"><option value="meter" selected>meter [m]</option><option value="exameter">exameter [Em]</option><option value="petameter">petameter [Pm]</option><option value="terameter">terameter [Tm]</option><option value="gigameter">gigameter [Gm]</option><option value="megameter">megameter [Mm]</option><option value="kilometer">kilometer [km]</option><option value="hectometer">hectometer [hm]</option><option value="dekameter">dekameter [dam]</option><option value="decimeter">decimeter [dm]</option><option value="centimeter">centimeter [cm]</option><option value="millimeter">millimeter [mm]</option><option value="micrometer">micrometer [&#181;m]</option><option value="micron">micron [&#181;]</option><option value="nanometer">nanometer [nm]</option><option value="picometer">picometer [pm]</option><option value="femtometer">femtometer [fm]</option><option value="attometer">attometer [am]</option><option value="megaparsec">megaparsec [Mpc]</option><option value="kiloparsec">kiloparsec [kpc]</option><option value="parsec">parsec [pc]</option><option value="light year">light year [ly]</option><option value="astronomical unit">astronomical unit [AU, UA]</option><option value="league">league [lea]</option><option value="nautical league (UK)">nautical league (UK)</option><option value="nautical league (international)">nautical league (international) [NL]</option><option value="league (statute)">league (statute) [st.league]</option><option value="mile">mile [mi, mi(Int)]</option><option value="nautical mile (UK)">nautical mile (UK) [NM (UK)]</option><option value="nautical mile (international)">nautical mile (international) [NM]</option><option value="mile (statute)">mile (statute) [mi, mi (US)]</option><option value="mile (US survey)">mile (US survey) [mi, mi (US)]</option><option value="mile (Roman)">mile (Roman)</option><option value="kiloyard">kiloyard [kyd]</option><option value="furlong">furlong [fur]</option><option value="furlong (US survey)">furlong (US survey) [fur]</option><option value="chain">chain [ch]</option><option value="chain (US survey)">chain (US survey) [ch]</option><option value="rope">rope</option><option value="rod">rod [rd]</option><option value="rod (US survey)">rod (US survey) [rd]</option><option value="perch">perch</option><option value="pole">pole</option><option value="fathom">fathom [fath]</option><option value="fathom (US survey)">fathom (US survey) [fath]</option><option value="ell">ell</option><option value="yard">yard [yd]</option><option value="foot">foot [ft]</option><option value="foot (US survey)">foot (US survey) [ft]</option><option value="link">link [li]</option><option value="link (US survey)">link (US survey) [li]</option><option value="cubit (UK)">cubit (UK)</option><option value="hand">hand</option><option value="span (cloth)">span (cloth)</option><option value="finger (cloth)">finger (cloth)</option><option value="nail (cloth)">nail (cloth)</option><option value="inch">inch [in]</option><option value="inch (US survey)">inch (US survey) [in]</option><option value="barleycorn">barleycorn</option><option value="mil">mil [mil, thou]</option><option value="microinch">microinch</option><option value="angstrom">angstrom [A]</option><option value="a.u. of length">a.u. of length [a.u., b]</option><option value="X-unit">X-unit [X]</option><option value="fermi">fermi [F, f]</option><option value="arpent">arpent</option><option value="pica">pica</option><option value="point">point</option><option value="twip">twip</option><option value="aln">aln</option><option value="famn">famn</option><option value="caliber">caliber [cl]</option><option value="centiinch">centiinch [cin]</option><option value="ken">ken</option><option value="Russian archin">Russian archin</option><option value="Roman actus">Roman actus</option><option value="vara de tarea">vara de tarea</option><option value="vara conuquera">vara conuquera</option><option value="vara castellana">vara castellana</option><option value="cubit (Greek)">cubit (Greek)</option><option value="long reed">long reed</option><option value="reed">reed</option><option value="long cubit">long cubit</option><option value="handbreadth">handbreadth</option><option value="fingerbreadth">fingerbreadth</option><option value="Planck length">Planck length</option><option value="Electron radius (classical)">Electron radius (classical)</option><option value="Bohr radius">Bohr radius [b, a.u.]</option><option value="Earth's equatorial radius">Earth's equatorial radius</option><option value="Earth's polar radius">Earth's polar radius</option><option value="Earth's distance from sun">Earth's distance from sun</option><option value="Sun's radius">Sun's radius</option></select></td></tr><tr><td colspan=2 style="padding:1.2ex 1.5ex;font-family:'Bebas';font-size:18px">Result:<br><input style="width:100%;font-family:Arial;font-size:100%" type=text name="type=result;bindid=left;bindid2=right"></td></tr></table>
</form>
<a href="http://www.unitconversion.org" class="linkunit">UnitConversion.org</a>
<script src=http://www.unitconversion.org/converter3/converter3.js></script>
 <!-- LENGTH CONVERTER END -->


	<a class="close-reveal-modal">&#215;</a>
</div>

<div id="moneda" class="reveal-modal small">
	<h3>Currency Converter</h3>
	<iframe width="300px" height="400px" frameborder="0" src="i-moneda.html"></iframe>
	<a class="close-reveal-modal">&#215;</a>
</div>

<div id="tiempo" class="reveal-modal">
	<h3>World Time Zone</h3>
<span id="normal_clock" class="wtb-ew-v1" style="width: 450px; display:inline-block"><script src="http://www.worldtimebuddy.com/clock_widget.js?h=0&bc=999999&cn=Universal+%2F+Greenwich+Time&wt=c2"></script><i><a href="http://www.worldtimebuddy.com/pst-to-est-converter">pst to est</a></i><noscript><a href="http://www.worldtimebuddy.com/pst-to-est-converter">pst to est</a></noscript><script>window[wtb_event_widgets.pop()].init()</script></span>   

	<span id="cel_clock" class="wtb-ew-v1" style="width: 204px; display:inline-block"><script src="http://www.worldtimebuddy.com/clock_widget.js?h=0&bc=8BA1BB&cn=Universal+%2F+Greenwich+Time&wt=c1"></script><i><a href="http://www.worldtimebuddy.com/pst-to-est-converter">pst to est</a></i><noscript><a href="http://www.worldtimebuddy.com/pst-to-est-converter">pst to est</a></noscript><script>window[wtb_event_widgets.pop()].init()</script></span>   
<a class="close-reveal-modal">&#215;</a>	
</div>
<script>
	$(document).ready(function() {
		$('#l5').addClass("activadote");
	});
</script>
</body>
</html>