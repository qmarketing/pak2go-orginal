<header class="headerWrap">
	<div class="header clearfix">
		<a href="." class="logo"><img src="images/pak2go.png" alt="Pak2Go" id="logohead" /></a>
		<div class="yellowbar"></div>
		<div class="headerInfo">
			<a href="en/" class="lang">English</a>
            <span class="tels"><img src="images/c-phone.png"/> 01800 APOYO 06</span>
			<a href="http://www.facebook.com/pak2go" target="_blank" class="social"><img src="images/c-facebook.png"/></a>
			<a href="http://www.twitter.com/pak2go" target="_blank" class="social"><img src="images/c-twitter.png"/></a>

		</div>
		
            <nav class="nav">
                <ul>
                    <li><a id="l1" href="empresa.php">Empresa</a></li>
                    <li><a id="l2" href="servicios-nacionales.php">Servicios Nacionales</a></li>
                    <li><a id="l3" href="servicios-internacionales.php">Servicios Internacionales</a></li>
                    <!-- <li><a id="l4" href="comercio-exterior.php">Comercio Exterior</a></li> -->
                    <li><a id="l5" href="recursos.php">Recursos</a></li>

                     <li class="dropdown">
                        <a id="l6" href="#" data-toggle="dropdown" class="dropdown-toggle">Contacto <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li class="nomar"><a href="contacto.php">Contacto</a></li>
                            <li class="nomar"><a href="bolsa-de-trabajo.php">Unete a Nuestro Equipo</a></li>
                        </ul>
                    </li>
                    <li><a id="l7" href="tour.php">Tour Virtual</a></li>
                </ul>
            </nav>

        <div class="logogptw">
            <img class="logogptwimg" src="images/greatplace.jpg">
        </div>

	</div>
</header> 

<nav class="navbar navbar-inverse navbar-fixed-top" id="rnav">
    <div class="container" id="rnav-cont">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        <a class="navbar-brand" href="#"><img src="images/pak2gohr.png" alt="Pak2Go" id="imgmenu"/></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav" id="navbar-ul">
                
                <li><a id="l1" href="empresa.php">Empresa</a></li>
                <li><a id="l2" href="servicios-nacionales.php">Servicios Nacionales</a></li>
                <li><a id="l3" href="servicios-internacionales.php">Servicios Internacionales</a></li>
                <li><a id="l4" href="comercio-exterior.php">Comercio Exterior</a></li> 
                <li><a id="l5" href="recursos.php">Recursos</a></li>
                <li><a id="l6" href="contacto.php">Contacto</a></li>
                <li><a id="l8" href="bolsa-de-trabajo.php">Unete a Nuestro Equipo</a></li>
                <li><a id="l7" href="tour.php">Tour Virtual</a></li>
                <li><a  href="en/" class="lang2">English</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
