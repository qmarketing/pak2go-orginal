<?php include('top.php'); ?>
<link rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/reveal2.css">

</head>
<body>
<?php include('header.php'); ?>

<div class="homeslides">
	<div class="frase"><h1><span>{</span>Tu envío es nuestro compromiso<span>}</span></h1></div>
	<div class="flexslider">
		<ul class="slides">
			<li>
				<img src="images/banner1.jpg" />
				<div class="flexCaption">
					<h1>- Tu socio logístico -</h1><h2>Ofrecemos la mejor solución en almacenaje <br>y distribución</h2>
				</div>
			</li>
			<li>
				<img src="images/banner2.jpg" />
				<div class="flexCaption">
					<h1>- Paquetería Local, Nacional e Internacional -</h1>
					<h2>Armamos tu pedido y lo distribuimos a cualquier parte <br>de la república por medio de las mejores <br>opciones de envío</h2>
				</div>
			</li>
			<li>
				<img src="images/banner3.jpg" />
				<div class="flexCaption">
					<h1>- Fletes -</h1>
					<h2>Terrestre, aéreo y marítimo. Nacional e internacional</h2>
				</div>
			</li>
			<li>
				<img src="images/banner4.jpg" />
				<div class="flexCaption">
					<h1>- Servicio internacional -</h1>
					<h2>Contacto local, enlace global</h2>
				</div>
			</li>
		</ul>
	</div>
	<!-- <div class="slideFlechita"><a href="#"></a></div> -->
</div>

<!-- pop -->
<a href="#" class="clickPop" data-reveal-id="pop">popup</a>
<div id="pop" class="reveal-modal xlarge">
	<div>
		<img src="images/fletes-small.jpg" alt="fletes-small" width="800" height="600" />
		<a href="contacto.php" title="Contactanos"></a>
	</div>
	<a class="close-reveal-modal"><span>Saltar intro</span>&#215;</a>
</div>
<!-- pop -->


<a href="servicios-nacionales.php">
<div class="bigNacionales">
	<div class="cont clearfix">
		<h2>Servicios<br>Nacionales</h2>
		<figure class="fig1"></figure>
		<figure class="fig2"></figure>
		<!-- <h3>Distribución de envío</h3> -->
	</div>
</div>
</a>
<a href="servicios-internacionales.php">
<div class="bigInternacionales">
	<div class="cont clearfix">
		<h2>Servicios<br>Internacionales</h2>
		<figure class="fig1"></figure>
		<figure class="fig2"></figure>
		<!-- <h3>Asesoría internacional</h3> -->
	</div>
</div>
</a>

<div class="dostres">
<div class="trespics clearfix">
	<div class="tresBlock bloque1">
		<div>Ofrecemos <br>servicio de almacenaje con distribución terrestre y día siguiente. (Enviamos sobres, cajas, tarimas, fletes completos, etc ...).</div><span></span>
	</div>
	
	<div class="tresBlock bloque2">
		<div>Controlamos totalmente tú almacen: Inventarios, Outsorcing de personal, servicios de empaquetado (Pick&Pack).</div><span></span>
	</div>
	
	<div class="tresBlock bloque3">
		<div>Monitoreamos tus envíos diariamente y te asignamos un asesor (Personalizado), quien tendrá el deber de mandarte un correo con eficiencias de servicio.</div><span></span>
	</div>
</div>

<div class="dospics clearfix">
	<div class="dosBlock bloque4">
		<div>Te damos asesoría logística para cualquier tipo de distribución.</div><span></span>
	</div>
	<div class="dosBlock bloque5">
		<div>Recolección <br>local, nacional <br>e internacional.</div><span></span>
	</div>
</div>
<div class="fotoTodos"></div>
</div>

<div class="greatplace">
	<img class="greatlogo" src="images/greatplace.jpg" alt="great-place-to-work"  />
</div>
 


<?php include('footer.php'); ?>

<script src="js/jquery.reveal.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/home.js"></script>


<!-- 
	<script src="js/jquery.scrollTo.js"></script>
 -->

</body>

</html>
	