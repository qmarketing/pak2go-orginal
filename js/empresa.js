$(document).ready(function() {

$(".empresaNav").sticky({topSpacing:0});

$('.clickQuienes').click(function() {
	$.scrollTo('.quienesSomos',1000);
	return false;
});

$('.clickMVV').click(function() {
	$.scrollTo('.mvv',1000);
	return false;
});

$('.clickHistoria').click(function() {
	$.scrollTo('.history',1000);
	return false;
});

$('.clickClientes').click(function() {
	$.scrollTo('.clientes',1000);
	return false;
});


$('.hslide').cycle({ 
    fx:     'scrollHorz', 
    speed:  500, 
    timeout: 0, 
    next:   '.hnext', 
    prev:   '.hprev' 
});

$(".clblock img").center();

});