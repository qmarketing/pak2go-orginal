$(document).ready(function() {


$('.flexslider').flexslider();

if(window.screen.width >768)
{
	if(sessionStorage.getItem('visto')!=1)
	{
		$('.clickPop').delay(1000).click();
		sessionStorage.setItem('visto',1);
	}
}
else
{
	$('.clickPop').css({ width: '0px' });
}

/* $('#tweets').tweetable({username: 'pak2go', time: false, limit: 3, replies: true, position: 'append'}); */

$('.slideFlechita').click(function() {
	 $.scrollTo('.bigNacionales',1000);
	 return false;
});


if(window.screen.width >768)
{
	$('.bigNacionales .fig2, .bigInternacionales .fig2').animate({ opacity:'0',top:'200' },0);

	function nacIn() {
		$('.bigNacionales').animate({ backgroundColor:'#FFF402' },300);
		$('.bigNacionales h2, .bigNacionales h3').animate({ color:'#484848' },300);
		$('.bigNacionales .fig1').animate({ opacity:'0',top:'-150' },300);
		$('.bigNacionales .fig2').animate({ opacity:'1',top:'55' },300);
	};
	function nacOut() {
		$('.bigNacionales').stop().animate({ backgroundColor:'#484848' },300);
		$('.bigNacionales h2, .bigNacionales h3').stop().animate({ color:'#FFFFFF' },300);
		$('.bigNacionales .fig1').stop().animate({ opacity:'1',top:'55' },300);
		$('.bigNacionales .fig2').stop().animate({ opacity:'0',top:'200' },300);
	};
	function interIn() {
		$('.bigInternacionales').animate({ backgroundColor:'#484848' },300);
		$('.bigInternacionales h2, .bigInternacionales h3').animate({ color:'#FFFFFF' },300);
		$('.bigInternacionales .fig1').animate({ opacity:'0',top:'-150' },300);
		$('.bigInternacionales .fig2').animate({ opacity:'1',top:'55' },300);
	};
	function interOut() {
		$('.bigInternacionales').stop().animate({ backgroundColor:'#FFF402' },300);
		$('.bigInternacionales h2, .bigInternacionales h3').stop().animate({ color:'#484848' },300);
		$('.bigInternacionales .fig1').stop().animate({ opacity:'1',top:'55' },300);
		$('.bigInternacionales .fig2').stop().animate({ opacity:'0',top:'200' },300);	
	};

	$('.bigNacionales').hover(function() {
		nacIn();
		interIn();
	},function() {
		nacOut();
		interOut();
	});

	 $('.tresBlock span, .tresBlock div, .dosBlock span, .dosBlock div').animate({ opacity:'0' },0);
	 $('.tresBlock div, .dosBlock div').animate({ top:'350' },0);

	$('.tresBlock, .dosBlock').hover(function() {
		  $('span',this).animate({ opacity:'1' },300);
		  $('div',this).animate({ top:'50', opacity:'1' },300,'easeOutExpo');
	},function() {
		  $('span',this).stop().animate({ opacity:'0' },300);
		 $('div',this).stop().animate({ top:'350', opacity:'1' },300);
	});
}

});
