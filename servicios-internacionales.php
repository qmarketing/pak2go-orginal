<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Fletes Internacionales Importacion y Exportacion</title>

<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<div class="sInterBanner">
	<h1>Servicios <br>Internacionales</h1>
	<a href="#"></a>
</div>

<section class="asesoriaInter clearfix">
	<span class="asesoriaAnchor"></span>
	<h2><span>{</span>Asesoría internacional<span>}</span></h2>
	<article class="as1">
		<span></span>
		<h3>¿No sabes por donde empezar? <br>¿Necesitas ayuda con la importación?</h3>
		<ul>
			<li>Te guiamos en el proceso de importación/exportación</li>
			<li>Te ayudamos a conseguir ese negocio en el extranjero</li>
			<li>Analizamos si es viable importar/exportar tus productos</li>
		</ul>
	</article>
	
	<article class="as2">
		<span></span>
		<h3>¿Confundido con la carga? <br>¿Necesito contenedor de 20’ o 40’? <br>¿Puedo asegurar mi mercancía? <br>¿Es mejor aéreo o marítimo?</h3>
		<ul>
			<li>Nosotros armamos tus envíos y aseguramos tu mercancía</li>
			<li>Coordinamos tus embarques en origen y destino</li>
			<li>Ofrecemos servicio puerta – puerta. Nosotros nos encargamos de toda la planeación</li>
		</ul>
	</article>
	
	<article class="as3">
		<span></span>
		<h3>¿No tienes visibilidad? <br>¿Usé la mejor ruta para importar / exportar?</h3>
		<ul>
			<li>Te asesoramos analizando la mejor ruta para disminuir costos</li>
			<li>Con nuestra tecnología te damos claridad y certeza en todo el proceso</li> 			
			<li>Tenemos experiencia y procesos bien definidos que hacen eficiente nuestra operación</li>
		</ul>
	</article>
</section>

<section class="transpInfo">
	<div class="cont clearfix">
		<article class="ti1 clearfix">
			<span></span>
			<h4>Transporte aereo</h4>
			<div class="col1">
				<h5>Express</h5>
				<ul>
					<li>Transportamos tu caja o pallet al instante, 1 día de tránsito.</li>
				</ul>
			</div>
			
			<div class="col2">
				<h5>Prioridad</h5>
				<ul>
					<li>Ideal para cargas críticas. De 1-2 días de tránsito.</li>
				</ul>
			</div>
			
			<div class="col3">
				<h5>Estándar</h5>
				<ul>
					<li>Para esas cargas importantes, pero no tan críticas. 5-6 días de tránsito.</li>
				</ul>
			</div>
		</article>
		
		<article class="ti2 clearfix">
			<h4>Transporte terrestre</h4>
			<div class="col1">
				<h5>Servicio dedicado</h5>
				<ul>
					<li>Manejamos tu carga por México, E.U. y Canadá</li>
					<li>Carga completa (FTL)</li>
					<li>Carga consolidada (LTL)</li>
				</ul>
			</div>
			
			<div class="col2">
				<h5>Servicios especiales</h5>
				<ul>
					<li>Manejo de productos farmacéuticos, a granel, químicos, electrónicos, de alto valor, peligrosos, etc.</li>
				</ul>
			</div>
			
			<div class="col3">
				<h5>Equipos</h5>
				<ul>
					<li>Caja seca</li>
					<li>Caja refrigerada Torton</li>
					<li>Rabón Plataformas</li>
				</ul>
			</div>			
		</article>
		
		<article class="ti3 clearfix">
			<h4>Transporte marítimo</h4>
			<div class="col1">
				<h5>Servicio FCL (Full Container Load)</h5>
				<ul>
					<li>Recolectamos tu mercancía en origen.</li>
					<li>Servicio de transporte marítimo por contenedor.</li>
				</ul>
			</div>
			<div class="col2">
				<h5>Servicio LCL (Less than Container Load)</h5>
				<ul>
					<li>Recolectamos tu mercancía en origen y consolidamos.</li>
					<li>Servicio desde una tarima.</li>
				</ul>
			</div>
			<div class="col3">
				<h5>Servicios especiales<br>&nbsp;</h5>
				<ul>
					<li>Con exceso de dimensiones, proyectos especiales de maquinaria pesada, producto a granel, etc.</li>
				</ul>
			</div>
		</article>
	</div>
</section>

<div class="internacional">
	<div class="internacionalStartC"></div>
	<div class="internacionalStart">Ver proceso internacional</div>
		
	<div class="clickMas beat" id="sipaso1">+</div>
	<div class="clickMas beat" id="sipaso2">+</div>
	<div class="clickMas beat" id="sipaso3">+</div>
	<div class="clickMas beat" id="sipaso4">+</div>
	<div class="clickMas beat" id="sipaso5">+</div>
	<div class="clickMas beat" id="sipaso6">+</div>
	<div class="clickMas beat" id="sipaso7">+</div>
	<div class="clickMas beat" id="sipaso8">+</div>
	
	<div class="isoTxt" id="sipaso1txt"><h2>Recolección en origen</h2><p>Recolectamos tu mercancía en fábrica o bodega.</p></div>
	<div class="isoTxt" id="sipaso2txt"><h2>Tu empresa</h2><p>Nos hacemos cargo de la transportación de tu mercancía a nuestros almacenes.</p></div>
	<div class="isoTxt" id="sipaso3txt"><h2>Carga de mercancía en origen</h2><p>Carga de contenedor o mercancía consolidada.</p></div>
	<div class="isoTxt" id="sipaso4txt"><h2>Aduana origen</h2><p>Despacho de exportación.</div>
	<div class="isoTxt" id="sipaso5txt"><h2>Transporte principal internacional</h2></div>
	<div class="isoTxt" id="sipaso6txt"><h2>Aduana destino</h2><p>Despacho de importación.</p></div>
	<div class="isoTxt" id="sipaso7txt"><h2>Descarga de mercancia <br>en destino</h2></div>
	<div class="isoTxt" id="sipaso8txt"><h2>Entrega en destino</h2></div>
	
	<div class="empezarinter"></div>
	
	<div class="piso"></div>
	<img src="images/pak2go-big.png" class="pak2gob2" width="440" height="73" />
	<img src="images/si-avion.png" class="si-avion" width="198" height="135" />
	<img src="images/si-caja.png" class="si-caja2" width="44" height="41" />
	<img src="images/si-caja.png" class="si-caja1" width="44" height="41" />
	<img src="images/si-caja.png" class="si-caja4" width="44" height="41" />
	<img src="images/si-caja.png" class="si-caja3" width="44" height="41" />
	<img src="images/si-camion1.png" class="si-camion1" width="38" height="38" />
	<img src="images/si-camion1.png" class="si-camion2" width="38" height="38" />
	<img src="images/si-camion1.png" class="si-camion3" width="38" height="38" />
	
	<img src="images/si-camion3.png" class="si-camion5" width="35" height="39" />
	<img src="images/si-camionblanco1.png" class="si-camionblanco1" width="51" height="49" />
	<img src="images/si-camionblanco1.png" class="si-camionblanco2" width="51" height="49" />
	<img src="images/si-camionblanco2.png" class="si-camionblanco3" width="50" height="47" />
	<img src="images/si-camionblanco2.png" class="si-camionblanco4" width="50" height="47" />
	<img src="images/si-casa1.png" class="si-casa1" width="59" height="80" />
	<img src="images/si-casa2.png" class="si-casa2-1" width="51" height="45" />
	<img src="images/si-casa2.png" class="si-casa2-2" width="51" height="45" />
	<img src="images/si-casa2.png" class="si-casa2-3" width="51" height="45" />
	<img src="images/si-casa2.png" class="si-casa2-4" width="51" height="45" />
	<img src="images/si-casa3.png" class="si-casa3" width="145" height="97" />
	<img src="images/si-casa4.png" class="si-casa4" width="75" height="181" />
	<img src="images/si-casa5.png" class="si-casa5" width="127" height="98" />
	<img src="images/si-casa5add.png" class="si-casa5add" width="86" height="89" />
	<img src="images/si-casa6.png" class="si-casa6" width="112" height="86" />
	<img src="images/si-casa7.png" class="si-casa7" width="119" height="92" />
	<img src="images/si-camion2.png" class="si-camion4" width="35" height="39" />
	<img src="images/si-flecha1.png" class="si-flecha1" width="26" height="17" />
	<img src="images/si-flecha2.png" class="si-flecha2" width="23" height="17" />
	<img src="images/si-flecha3.png" class="si-flecha3" width="27" height="19" />
	<img src="images/si-flecha2.png" class="si-flecha4" width="23" height="17" />
	<img src="images/si-flecha3.png" class="si-flecha5" width="27" height="19" />
	<img src="images/si-flecha2.png" class="si-flecha6" width="23" height="17" />
	<img src="images/si-grua1.png" class="si-grua1" width="22" height="22" />
	<img src="images/si-grua1.png" class="si-grua2" width="22" height="22" />
	<img src="images/si-tarimas.png" class="si-tarima1" width="42" height="32" />
	<img src="images/si-tarimas.png" class="si-tarima2" width="42" height="32" />
	<img src="images/logos-int.png" class="logos-int" width="233" height="107" />
</div>

<?php include('footer.php'); ?>

<script src="js/easing.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/inter.js"></script>
<script>
	$(document).ready(function() {
		$('#l3').addClass("activadote");
	});
</script>
</body>
</html>