<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Fletes Mexico</title>

<?php include('top.php'); ?>

</head>
<body>
<?php include('header.php'); ?>

<div class="sNacBanner">
	<h1>Servicios <br>Nacionales</h1>
	<a href="#"></a>
</div>


<section class="fdInfo">
	<div class="conts clearfix">
		<h2>Fletes y <br>distribución</h2>
		<div class="more">
			<h3>Distribución local, nacional e internacional de su mercancía por medio de las siguientes opciones.</h3>
			<ul>
				<li>Paquetería local, nacional e internacional, terrestre y aéreo.</li>
				<li>Carga Consolidada a cualquier parte de la república.</li>
				<li>Flete directo local y nacional. (1.5 tons, 3.5 tons, 10 tons y Trailer 48 y 53 pies)</li>
				<li>Damos seguimiento personalizado de todos tus movimientos, asignando una persona a tu cuenta, responsable de tu plena satisfacción (sin costo adicional).</li>
				<li>Contamos con alianzas estratégicas con empresas de transporte de gran prestigio y calidad.</li>
			</ul>
		</div>
		<div class="figFD">
			<img src="images/fd1.png" class="fd1" width="135" height="150" />
			<img src="images/fd2.png" class="fd2" width="249" height="278" />
			<img src="images/fd3.png" class="fd3" width="348" height="388" />
			<img src="images/fd4.png" class="fd4" width="416" height="464" />
			<img src="images/fd5.png" class="fd5" width="492" height="548" />
			<img src="images/fd6.png" class="fd6" width="566" height="632" />
			<img src="images/fd7.png" class="fd7" width="626" height="698" />
		</div>
	</div>
</section>

<section class="almacenaje">
	<div class="linea1"></div>
	 <div class="conts clearfix">
		<h2>Almacenaje</h2>
		<ul>
			<li>Contamos con más de 5000m2 en la ciudad de Monterrey.</li>
			<li>Almacenaje de inventario desde un espacio de 25m m2.</li>
			<li>Control de inventarios.</li>
			<li>Sistema de seguridad 24/7.</li>
			<li>Circuito cerrado de máxima seguridad.</li>
			<li>Carga y descarga de unidades.</li>
			<li>Servicios de maniobras.</li>
		</ul>
	</div> 
</section>

<section class="pickpack">
	<div class="linea2"></div>
	<div class="conts">
		<h2>Pick & Pack</h2>
		<h3>Surtido y armado de pedidos</h3>
		<p>Como parte del servicio integral nuestro personal esta preparado para surtir y armar pedidos con estrictas reglas de calidad, tomando en cuenta procesos impuestos por el cliente.<br>Por medio de órdenes de compra se hará el surtido de sus pedidos y se empacara listo para enviarse a donde se requiera.</p>
	</div>
</section>

<section class="asesores">
	<div class="linea3"></div>
	<div class="conts clearfix">
		<article class="txt">
			<h2>Asesores <br>Logísticos</h2>
			<p>Somos expertos en la materia, te asesoramos para que tu almacén y tu distribución se manejen de la manera más eficiente, rápida y segura. Sobre todo con un enfoque en reducción de costos y rentabilidad para tu empresa.</p>
		</article>
	</div>
	</div>
</section>

<div class="nacional">
	<div class="linea4"></div>
	
	<div class="isoNacional">
		<div class="nacionalStartC"></div>
		<div class="nacionalStart">Ver proceso nacional</div>
		
		<div class="clickMas beat" id="snpaso1">+</div>
		<div class="clickMas beat" id="snpaso2">+</div>
		<div class="clickMas beat" id="snpaso3">+</div>
		<div class="clickMas beat" id="snpaso4">+</div>
		
		<div class="isoTxt" id="snpaso1txt">
			<h2>PAK2GO</h2><p>Coloca un pedido, podemos mover paquetes, tarimas, flete directo local y nacional.</p>
		</div>
		<div class="isoTxt" id="snpaso2txt">
			<h2>A tu empresa</h2><p>Nos hacemos cargo de la transportación de tu mercancía a nuestros almacenes.</p>
		</div>
		<div class="isoTxt" id="snpaso3txt">
			<h2>Almacenaje</h2><p>Almacenamos, empacamos y etiquetamos tomando en cuenta los procesos impuestos por tu empresa.</p>
		</div>
		<div class="isoTxt" id="snpaso4txt">
			<h2>Distribución</h2><p>Distribuimos a cualquier parte de la república, dando seguimiento personalizado de todos tus movimientos.</p>
		</div>
		
		<div class="empezarnac"></div>
		
		<div class="piso"></div>
		<img src="images/sn-avion.png" class="sn-avion" />
		<img src="images/sn-cajas.png" class="sn-caja1" />
		<img src="images/sn-cajas.png" class="sn-caja2" />
		<img src="images/sn-camion1.png" class="sn-camion1" />
		<img src="images/sn-camion3.png" class="sn-camion3" />
		<img src="images/sn-camion4.png" class="sn-camion4" />
		<img src="images/sn-camion4.png" class="sn-camion4-1" />
		<img src="images/sn-camion5.png" class="sn-camion5" />
		<img src="images/sn-casa1.png" class="sn-casa1" />
		<img src="images/sn-casa2.png" class="sn-casa2" />
		<img src="images/sn-casa2.png" class="sn-casa2-1" />
		<img src="images/sn-casa2.png" class="sn-casa2-2" />
		<img src="images/sn-casa3.png" class="sn-casa3" />
		<img src="images/sn-casa4.png" class="sn-casa4" />
		<img src="images/sn-casa5.png" class="sn-casa5" />
		<img src="images/sn-camion2.png" class="sn-camion2" />
		<img src="images/sn-flecha1.png" class="sn-flecha1" />
		<img src="images/sn-flecha2.png" class="sn-flecha2" />
		<img src="images/sn-flecha3.png" class="sn-flecha3" />
		<img src="images/sn-flecha4.png" class="sn-flecha4" />
		<img src="images/sn-grua.png" class="sn-grua" />
		<img src="images/pak2go-big.png" class="pak2gob1"/>
		<img src="images/logos-nacional.png" class="logos-nacional"/>
	</div>
	
</div> 

<?php include('footer.php'); ?>

<script src="js/easing.js"></script>
<script src="js/center.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/nacionales.js"></script>
<script>
	$(document).ready(function() {
		$('#l2').addClass("activadote");
	});
</script>
</body>
</html>