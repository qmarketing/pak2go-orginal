<!DOCTYPE html>
<html lang="es">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Mudanzas Fletes y Regresos</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge;chrome=1">

<meta name="description" content="Fletes,Paqueteria, Distribución local, Carga Consolidada, nacional e internacional de su mercancía terrestre y aéreo">
<meta name="keywords" content="Fletes, Plataformas, cajas secas, Mudanzas, low boys, Carga, descarga, Flete, local, nacional, mudanza, transporte, terrestre, plataforma, camion, tracto, trailer, torton, rabon, camioneta, consolidado, distribucion, logistica, carga, directo, foraneo, seguro, servicio, federal, redilas, camion 3.5, tres y media toneladas, autotransporte, express, economico, mexico, transportar, caja, urgente, flotilla, especiales, caja seca, cotizar, Transfer, Transferencia, Operador, tarima, dedicado, almacenaje, solucion, importacion, exportacion, operacion, terrestre, aereo, maquila, rapido,empresa, movimiento, maquinaria, toneladas, pies, norte, noreste, bajio, occidente, paqueteria, mensajeria">
<meta name="format-detection" content="telephone=no">
<meta name="robots" content="INDEX,FOLLOW,ARCHIVE" />


<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<link rel="shortcut icon" href="favicon.ico">
<link rel="apple-touch-icon-precomposed" href="./apple-touch-icon.png" />
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="css/main.css">

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script> -->

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>


<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40661439-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<!-- Hotjar Tracking Code for www.pak2go.com -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:63598,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>
